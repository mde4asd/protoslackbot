"""
The main application for the Slack Bot
receives events using Slack Bolt Framework
listeners. Choice between vanilla pipeline classification
or classification using the Fine Tuned model
"""

import logging

logging.basicConfig(format='%(asctime)s: %(levelname)s: %(module)s.%(funcName)s - %(message)s', level=logging.DEBUG)

import os
from slack_bolt import App
from slack_bolt.adapter.flask import SlackRequestHandler
from slack_bolt.oauth.oauth_settings import OAuthSettings
from data.stores.CustomInstallationStore import CustomInstallationStore
from data.stores.CustomOAuthStateStore import CustomOAuthStateStore
from data import Database
from dotenv import load_dotenv
from model.ClassifierFT import Classifier
from util import SlackEventParser
from data.stores.DesignDecisionStore import DesignDecisionStore
from util import BlockBuilder
from model.Candidate import Candidate
from flask import Flask, request, render_template
from waitress import serve
from data.stores.WikiConfigurationStore import WikiConfigurationStore
from data.entities import WikiConfigurationParser, DesignDecisionParser
from outputters.GitLabOutputter import create_gitlab_outputter
from util import Pagination
from data.stores.UserStore import UserStore

load_dotenv()

logger = logging.getLogger(__name__)

client_id, client_secret, signing_secret = (
    os.environ.get("SLACK_CLIENT_ID"),
    os.environ.get("SLACK_CLIENT_SECRET"),
    os.environ.get("SLACK_SIGNING_SECRET")
)

engine = Database.setup_db()

installation_store = CustomInstallationStore(
    client_id=client_id,
    engine=engine,
    logger=logger
)
oauth_state_store = CustomOAuthStateStore(
    expiration_seconds=120,
    engine=engine,
    logger=logger
)
wiki_configuration_store = WikiConfigurationStore(
    engine=engine,
    logger=logger
)
user_store = UserStore(
    engine=engine,
    logger=logger
)
design_decision_store = DesignDecisionStore(
    engine=engine,
    user_store=user_store,
    logger=logger
)

app = App(
    logger=logger,
    signing_secret=signing_secret,
    installation_store=installation_store,
    oauth_settings=OAuthSettings(
        client_id=client_id,
        client_secret=client_secret,
        state_store=oauth_state_store,
        scopes=[
            "app_mentions:read", "chat:write", "commands", "incoming-webhook", "users:read", "channels:history",
            "groups:history", "mpim:history", "im:history"
        ]
    )
)

CLASSIFIER = Classifier()


@app.event("app_mention")
def app_mention(ack, event, say, client):
    """
    Receives user messages when @bot tagged. Since, the message represents a design decision it is stored in the
    database, then its parts are classified and sent back to the user for manual analysis of the classifications and
    finally the design decision may be outputted to a wiki (if this has been set up)
    :param ack: acknowledge message received
    :param event: all data from message and channel
    :param say: used for replying in channel
    :param client: a WebClient that can be used to make a call to any Slack Web API method
    """
    ack()
    try:
        message = SlackEventParser.sanitise_user_input(event["text"])
        thread = event['event_ts']
        channel = event['channel']
        user_id = event['user']
        user = {
            "slack_id": user_id,
            "name": client.users_info(user=user_id)["user"]["profile"]["real_name_normalized"]
        }

        if not message:
            say(text=f"Hey <@{user_id}>! It looks like you want to chat with me but I didn't get a message",
                thread_ts=thread)
        else:
            decision_id = design_decision_store.get_design_decision_id_by_message_id(thread)
            logger.info(f"... checking to see if design decision exists for message with id {thread}")

            if decision_id:
                design_decision_store.amend_design_decision(decision_id, message, user, thread)
                logger.info("...user updated message containing design decision therefore updated design decision")
            else:
                decision_id = design_decision_store.create_design_decision(event, user)
                logger.info("...received @mention event, stored design decision")

            if 'edited' in event:
                SlackEventParser.delete_messages_in_thread(client, channel, thread)
                logger.debug("... removing previous classifications from thread")

            output_classifications(message=message, say=say, thread=thread)
            logger.info("...complete classification")

            output_to_wiki(decision_id, user, event["team"], message, thread, say)
            logger.info("...handled wiki output")
    except Exception as e:
        logger.error(e)
        say(
            text="An error occurred when attempting to communicate with me, "
                 "and therefore, your design decision could not be saved.",
            thread_ts=thread
        )


@app.action("checkbox_select")
def hello(ack, body, say, payload):
    """
    Listener that receives POST for action taken on checkbox by user
    :param ack: requirement for slack to acknowledge receipt
    :param body: body of message from slack API
    :param say: the framework required to speak back to user
    :param payload:
    :return: NONE: Sends message back to client through SAY
    """
    ack()
    thread = body['container']['thread_ts']

    wiki_configuration = wiki_configuration_store.get_wiki_configuration(body["team"]["id"])
    gitlab_outputter = create_gitlab_outputter(wiki_configuration=wiki_configuration, logger=logger)
    page, error = gitlab_outputter.setup_wiki_page()

    checked_item = SlackEventParser.get_selected_data(payload['selected_options'])
    if not checked_item:
        sentence = SlackEventParser.get_changed_item(payload['block_id'], body['message']['blocks'])
        decision_id, label = design_decision_store.delete_design_decision_component_by_content(sentence, thread)
        if label and page:
            gitlab_outputter.output_component(decision_id, sentence, label)
        say(thread_ts=thread, text=f":skull_and_crossbones: `{sentence}` selection removed")
    else:
        sentence = next(iter(checked_item))
        label = checked_item[sentence].replace('_', ' ')
        if label.startswith("None"):
            all_options = Candidate.generate_list()
            say(blocks=BlockBuilder.build_reply_block(all_options, True, changed=sentence), thread_ts=thread,
                text=f":construction_worker: Feature under construction {sentence}")
        elif label.startswith("NA"):
            logger.debug(f"Classification cannot be made for ({label}): {sentence}")
            say(thread_ts=thread,
                text=f":interrobang:N/A: \n `{sentence}`")
        else:
            logger.debug(f"CHECKED BOX: {sentence} {label}")
            decision_id, decision_component_id = design_decision_store.create_design_decision_component(sentence, label, thread)
            if page:
                gitlab_outputter.output_component(decision_id, sentence, label)
            say(thread_ts=thread,
                text=f":card_file_box:Store: \n {label.capitalize()} `{sentence}`")


@app.command("/setup_wiki")
def setup_wiki(ack, say, command):
    """
    This method handles the /setup_wiki command used to set up the wiki configuration for a Slack team.
    The method first makes a call to parse the incoming command to get its inputs before saving the wiki configuration
    to the database. The user in Slack is then told whether the configuration was successful or not.
    The command accepts inputs of the form "/setup_wiki <host_url>, <project_id>, <slug>"
    :param ack: requirement for slack to acknowledge receipt
    :param say: the framework required to speak back to user
    :param command: the received command (with its inputs) from Slack.
    :return:  NONE: Sends message back to client through SAY
    """
    ack()
    try:
        configuration = WikiConfigurationParser.parse_configuration(command)
        gitlab_outputter = create_gitlab_outputter(wiki_configuration=configuration, logger=logger)
        page, error = gitlab_outputter.setup_wiki_page()
        if page:
            wiki_configuration_store.save_wiki_configuration(configuration)
            say("Wiki configuration updated.")
        else:
            say(f"Wiki configuration is invalid and has not been updated ({error}).")
    except Exception as e:
        logger.error(e)
        say("An error occurred when updating your wiki configuration.")


@app.command("/search_decisions")
def search_decisions(ack, say, command):
    """
    This method handles the /search_decisions command used to search for design decisions saved in the database.
    The method returns the first 10 design decisions matching the search criteria.
    The command accepts inputs of the form "/search_decisions <query_string>"
    :param ack: requirement for slack to acknowledge receipt
    :param say: the framework required to speak back to user
    :param command: the received command (with its inputs) from Slack.
    :return:  NONE: Sends message back to client through SAY
    """
    ack()
    try:
        results, total_rows = design_decision_store.search_design_decisions(command["team_id"], command["text"])
        say(blocks=BlockBuilder.build_search_result_block(results, command["text"], total_rows), text="search results")
    except Exception as e:
        logger.error(e)
        say("Something went wrong when searching... Please try again.")


@app.action("show_more_button")
def show_more(ack, body, say, client):
    """
    This method handles the response for a user clicking on the Show More button in Slack.
    The method retrieves "more" design decisions using pagination. It then outputs the
    additional design decisions to Slack. Finally, it updates the original message to remove
    the Show More button to prevent users in Slack spamming it.
    :param ack: requirement for Slack to acknowledge receipt
    :param body: body of message from Slack API
    :param say: the framework required to speak back to user
    :param client: a WebClient that can be used to make a call to any Slack Web API method
    :return: None: Sends message back to client through SAY
    """
    ack()
    try:
        team_id = body["team"]["id"]
        page = int(body["message"]["blocks"][-1]["accessory"]["value"].split("|~|")[1])
        query_string = body["message"]["blocks"][-1]["accessory"]["value"].split("|~|")[0]
        results, total_rows = design_decision_store.search_design_decisions(team_id, query_string, page)
        say(blocks=BlockBuilder.build_search_result_block(results, query_string, total_rows, page),
            text="search results")
        client.chat_update(
            channel=body["container"]["channel_id"],
            ts=body["container"]["message_ts"],
            blocks=Pagination.remove_pagination_section(body),
            text="search results"
        )
    except Exception as e:
        logger.error(e)
        say("Something went wrong when retrieving more design decisions")


@app.command("/amend_decision")
def amend_decision(ack, say, command, client):
    """
    This method listens for the /amend_decision command.
    1) When it receives a command it will first get the inputs of the command.
    2) Next it retrieves the name of the user who used the command by making an API call to the Slack
    API as this information is not included with the command.
    3) The design decision trying to be edited is then checked for its existence.
    4) If the design decision does not exist an error message is sent to the user otherwise ...
    5) The design decision is updated in the database and on the wiki. The classifications for the design decision
    also need to be updated
    :param ack: requirement for Slack to acknowledge receipt
    :param say: the framework required to speak back to user
    :param command: the received command (with its inputs) from Slack
    :param client: a WebClient that can be used to make a call to any Slack Web API method
    :return: None: Sends message back to client through SAY
    """
    ack()
    try:
        decision_id, design_decision = DesignDecisionParser.parse_amend_command(command)
        user_id = command['user_id']
        user = {
            "slack_id": user_id,
            "name": client.users_info(user=user_id)["user"]["profile"]["real_name_normalized"]
        }

        existing_decision = design_decision_store.get_design_decision_by_id(decision_id)
        if existing_decision:
            response = client.chat_postMessage(
                channel=command["channel_id"],
                text=f"Your design decision ({decision_id}) is being updated..."
            )
            thread = response.data['ts']

            design_decision_store.amend_design_decision(decision_id, design_decision, user, thread)
            logger.info("...received /amend_decision command, updated design decision")

            output_classifications(message=design_decision, say=say, thread=thread, decision_id=decision_id)
            logger.info("...complete updated classification")

            output_to_wiki(decision_id, user, command["team_id"], design_decision, thread, say)
            logger.info("...updated design decision on wiki")
        else:
            say(
                f"I can not update your design decision. "
                f"An existing design decision with an id of {decision_id} does not exist."
            )
    except Exception as e:
        logger.error(e)
        say("Something went wrong when amending your design decision.")


def output_classifications(message, say, thread, decision_id=None):
    """
    The output of the classifications is wrapped in a try except block because a design decision may not always be
    able to be classified
    :param message: the original message containing the design decision
    :param say: used for replying in channel
    :param thread: the Slack thread to reply the classifications in
    :param decision_id: the id of the stored design decision
    """
    try:
        classified_decisions = CLASSIFIER.classify_user_input(message)
        say(
            blocks=BlockBuilder.build_reply_block(classified_decisions, decision_id=decision_id),
            thread_ts=thread,
            text="classifications"
        )
    except Exception as e:
        logger.error(e)
        say(text="Your design decision could not be classified but has been saved/updated.", thread_ts=thread)


def output_to_wiki(decision_id, user, team_id, design_decision, thread, say):
    """
    This method outputs a design decision to a GitLab wiki. The method first gets the wiki configuration
    for the team. If a page can not be retrieved from the wiki then the wiki has not been configured correctly.
    The team will either be informed the output was successful or not
    :param decision_id: the id of the stored design decision
    :param user: the user who recorded the design decision
    :param team_id: the id of the team who the user belongs to
    :param design_decision: the textual representation of the design decision
    :param thread: the thread the original question occurred in
    :param say: used for replying in channel
    :return: None, as outputting to wiki and replying in channel.
    """
    wiki_configuration = wiki_configuration_store.get_wiki_configuration(team_id)
    gitlab_outputter = create_gitlab_outputter(wiki_configuration=wiki_configuration, logger=logger)
    page, error = gitlab_outputter.setup_wiki_page()
    if page:
        gitlab_outputter.output_decision(decision_id, user["name"], design_decision)
        wiki_url = gitlab_outputter.get_wiki_url()
        say(thread_ts=thread, text=f"Your design decision has been outputted. Check your <{wiki_url}|wiki> now!")
    else:
        logger.error(error)
        say("Your design decisions are not being outputted because your wiki is incorrectly configured. You "
            "can use the /setup_wiki command to configure your wiki so I can output your design decisions "
            "there.")


flask_app = Flask(__name__)
handler = SlackRequestHandler(app)


@flask_app.route("/slack/events", methods=["POST"])
def slack_events():
    return handler.handle(request)


@flask_app.route("/slack/install", methods=["GET"])
def install():
    return handler.handle(request)


@flask_app.route("/slack/oauth_redirect", methods=["GET"])
def oauth_redirect():
    return handler.handle(request)


@flask_app.route("/", methods=["GET"])
def home_page():
    url = "{}/slack/install".format(os.environ.get("HOST_URL"))
    return render_template("index.html", url=url)


def main():
    port = int(os.environ.get("PORT", 3000))
    serve(flask_app, host="0.0.0.0", port=port)
    logger.info("...started on port %s", port)


if __name__ == "__main__":
    main()
