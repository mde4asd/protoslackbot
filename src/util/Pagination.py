import math

PAGE_SIZE = 5


def pagination_required(total_rows, page):
    total_pages = math.ceil(total_rows / PAGE_SIZE)
    return (page + 1) < total_pages


def create_pagination_section(query_string, page):
    return {
        "type": "section",
        "text": {
            "type": "mrkdwn",
            "text": "More design decisions are available.",
        },
        "accessory": {
            "type": "button",
            "text": {
                "type": "plain_text",
                "text": "Show More"
            },
            "action_id": "show_more_button",
            "value": query_string + "|~|" + str(page)
        }
    }


def remove_pagination_section(body):
    return body["message"]["blocks"][:-1]
