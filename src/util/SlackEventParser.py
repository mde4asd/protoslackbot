import re
import logging

logger = logging.getLogger(__name__)


def get_selected_data(selected_options):
    """
    Helper function to de-tangle the actions undertaken on a checkbox
    :param selected_options: the options that are available to the user (checkboxes)
    :return: list of options that were modified
    """
    logger.debug(f"Get selected/deselected checkbox value")
    options = dict()
    try:
        for option in selected_options:
            key, value = option['value'].split('|')
            options[key] = value
    except TypeError as e:
        logger.debug(f"Detected additional | value, RIP Error {e}")
    return options


def get_changed_item(changed_block, blocks):
    """
    Used for the deselection of a checkbox as can be a little tricky
    to find the sentence data
    :param changed_block:  the ID of the block that was changed
    :param blocks: a small selection of the body containing blocks
    :return:
    """
    for block_number in blocks[2:]:
        if block_number['block_id'] == changed_block:
            sentence = block_number['text']['text']
            return sentence


def sanitise_user_input(user_input):
    """
    User message arrives with a user code find, strip, and return message only. Newlines are also stripped
    as they are not correctly formatted in the wiki output
    :param user_input: message from user with <@user_code> tagged bot
    :return: plain string with only message content no user tag
    """
    bot_tag = '<@.+?>'
    message = re.sub(bot_tag, '', user_input)
    message = message.replace("\n", " ")
    return message.strip()


def delete_messages_in_thread(client, channel_id, message_id):
    """
    This method deletes all messages sent by the bot in a thread. The method first gets all messages in the thread
    before deleting the messages sent by the bot. The purpose of this method is to declutter the thread when
    a message representing a design decision is edited as the classifications will be different
    :param client: a WebClient that can be used to make a call to any Slack Web API method
    :param channel_id: the id of the Slack channel the message occurred in
    :param message_id: the id of the Slack message
    :return: None, as making calls to the Slack API using the client.
    """
    messages_in_thread = client.conversations_replies(
        channel=channel_id,
        ts=message_id
    )
    for message in messages_in_thread['messages']:
        if 'bot_id' in message:
            client.chat_delete(
                channel=channel_id,
                ts=message['ts']
            )
