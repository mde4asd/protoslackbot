"""
Methods to build reply blocks for the Slack API
"""

import logging
from util import SlackEventParser

from util import Pagination

DATE_INDEX = 3
TEXT_INDEX = 1
NAME_INDEX = -1
ID_INDEX = 0


def build_reply_block(option_dict, failure_case=False, changed=None, decision_id=None):
    """
    Make use of python blocks to format the reply in chat:
    in the format Section | Option/s & Abort Buttons
    :param option_dict: Default Dict containing Key: Sentence Values = [Top3 Labels]
    :param failure_case: True when the classifier doesn't produce a hit (user defined)
    :param changed: mark if the user has selected 'none of these' as different workflow
    :param decision_id: the id of the design decision that has been updated (an optional parameter as a block will not
    always need to be built for a design decision that has been updated)
    :return: formatted blocks that slack likes for display
    """
    logging.info('... building blocks')
    blocks = []

    first_text = "Hey, it looks like you want me to add a design decision!: "
    sad_text = "Oops, looks like I need some training here is all the options"
    update_text = f"Hey, it looks like you need to update the classifications for your design decision ({decision_id})!:"

    if not failure_case:
        if decision_id:
            blocks.append(section_block_builder(update_text))
        else:
            blocks.append(section_block_builder(first_text))
        blocks.append(divider_block_build())
        # Iteration of UI involving checkbox fields
        for key, value_list in option_dict.items():
            blocks.append(checkbox_block_builder(key, value_list[0]))
    else:
        blocks.append(section_block_builder(sad_text))
        blocks.append(divider_block_build())
        blocks.append(all_options_cb_block_builder(changed, option_dict))

    logging.info('... block building complete')
    return blocks


def divider_block_build():
    """
    Simply a line that divides blocks
    :return: dividing line JSON
    """
    return {"type": "divider"}


def section_block_builder(section_text):
    """
    Helper function to build block section text
    :param section_text: text for the section
    :return: block representing the section as JSON
    """
    block_section = {
        "type": "section",
        "text": {
            "type": "plain_text",
            "text": section_text
        }
    }
    return block_section


def build_input_fields(label, clause_list):
    """
    Representing the input fields for the slack reply
    :param label: the label for the field
    :param clause_list: the classified text clauses, filled as hints
    :return: a formatted JSON block for input to reply
    """
    s = ' '.join([str(elem) for elem in clause_list])
    input_field = {
        "type": "input",
        "element": {
            "type": "plain_text_input",
            "multiline": True,
            "action_id": "plain_text_input-action",
            "placeholder": {
                "type": "plain_text",
                "text": s
            }
        },
        "label": {
            "type": "plain_text",
            "text": label,
            "emoji": True
        },
        "hint": {
            "type": "plain_text",
            "text": "use the classified text or edit"
        }
    }
    return input_field


def button_block_builder(label, value="TBC", link="https://google.com", style="primary"):
    """
    Helper function to build buttons from results of classification
    :param label: label for buttons
    :param value: TODO
    :param link: will be the endpoint to apply action for said button
    :param style: can change the colour/style
    :return: formatted JSON representing button
    """
    block_button = {
        "type": "actions",
        "elements": [
            {
                "type": "button",
                "style": style,
                "text": {
                    "type": "plain_text",
                    "text": label,
                },
                "value": value,
                "url": link
            }
        ]
    }
    return block_button


def context_block_builder(text):
    """
    The starting block for the block builder
    :param text: TODO:
    :return: formatted JSON Block for context
    """
    block = {
        "type": "context",
        "elements": [
            {
                "type": "image",
                "image_url": "https://pbs.twimg.com/profile_images/625633822235693056/lNGUneLX_400x400.jpg",
                "alt_text": "cute cat"
            },
            {
                "type": "plain_text",
                "text": text
            }
        ]
    }
    return block


def text_block_builder():
    """
    Helper function to build a plain text entry box for reply messages
    :return: JSON formatted text block
    """
    plain_text_block = {
        "type": "input",
        "block_id": "input123",
        "label": {
            "type": "plain_text",
            "text": "Other"
        },
        "element": {
            "type": "plain_text_input",
            "action_id": "plain_input",
            "placeholder": {
                "type": "plain_text",
                "text": "Enter a different option here"
            }
        }
    }
    return plain_text_block


def checkbox_block_builder(sentence_text, option_list):
    option_1, option_2, option_3 = option_list[0], option_list[1], option_list[2]
    checkbox_block = {
        "type": "section",
        "text": {
            "type": "mrkdwn",
            "text": sentence_text
        },
        "accessory": {
            "type": "checkboxes",
            "options": [
                {
                    "text": {
                        "type": "mrkdwn",
                        "text": option_1
                    },
                    "value": f"{sentence_text}|{option_1}"
                },
                {
                    "text": {
                        "type": "mrkdwn",
                        "text": option_2
                    },
                    "value": f"{sentence_text}|{option_2}"
                },
                {
                    "text": {
                        "type": "mrkdwn",
                        "text": option_3
                    },
                    "value": f"{sentence_text}|{option_3}"
                },
                {
                    "text": {
                        "type": "mrkdwn",
                        "text": "None of these"
                    },
                    "value": f"{sentence_text}|None"
                }
            ],
            "action_id": 'checkbox_select'
        }
    }
    return checkbox_block


def all_options_cb_block_builder(sentence_text, option_list):
    sentence, = sentence_text.split('|')
    checkbox_block = {
        "type": "section",
        "text": {
            "type": "mrkdwn",
            "text": sentence
        },
        "accessory": {
            "type": "checkboxes",
            "options": [
                {
                    "text": {
                        "type": "mrkdwn",
                        "text": option_list[0]
                    },
                    "value": f"{sentence}|{option_list[0]}"
                },
                {
                    "text": {
                        "type": "mrkdwn",
                        "text": option_list[1]
                    },
                    "value": f"{sentence}|{option_list[1]}"
                },
                {
                    "text": {
                        "type": "mrkdwn",
                        "text": option_list[2]
                    },
                    "value": f"{sentence}|{option_list[2]}"
                },
                {
                    "text": {
                        "type": "mrkdwn",
                        "text": option_list[3]
                    },
                    "value": f"{sentence}|{option_list[3]}"
                },
                {
                    "text": {
                        "type": "mrkdwn",
                        "text": option_list[4]
                    },
                    "value": f"{sentence}|{option_list[4]}"
                },
                {
                    "text": {
                        "type": "mrkdwn",
                        "text": option_list[5]
                    },
                    "value": f"{sentence}|{option_list[5]}"
                },
                {
                    "text": {
                        "type": "mrkdwn",
                        "text": option_list[6]
                    },
                    "value": f"{sentence}|{option_list[6]}"
                },
                {
                    "text": {
                        "type": "mrkdwn",
                        "text": option_list[8]
                    },
                    "value": f"{sentence}|{option_list[8]}"
                },
            ],
            "action_id": 'checkbox_select'
        }
    }

    return checkbox_block


def build_search_result_block(results, query_string, total_rows, page=0):
    """
    Builds blocks of text that are included as a response to the search performed by a Slack user
    :param results: The results of the search performed by the user
    :param query_string: The query string from the search
    :param total_rows: the total number of results needed for pagination
    :param page: the current page number needed for pagination
    :return: blocks, a list of blocks that make up a message that will be sent as a response to the Slack user
    """
    logging.info('... building search result blocks')
    blocks = []

    if page == 0:
        first_block = {
            "type": "section",
            "text": {
                "type": "mrkdwn",
                "text": f":mag: The results of the search: _{query_string}_"
            }
        }
        blocks.append(first_block)
        blocks.append(divider_block_build())

    if (results is not None) and (len(results) > 0):
        if page == 0:
            header_block = {
                "type": "section",
                "text": {
                    "type": "mrkdwn",
                    "text": "*Date* | *Recorded By* | *Decision Id* | *Design Decision*"
                }
            }
            blocks.append(header_block)
            blocks.append(divider_block_build())

        for result in results:
            section_text = "| {} | {} | {} |\n{}".format(
                result[DATE_INDEX].strftime("%d/%m/%Y"),
                result[NAME_INDEX],
                result[ID_INDEX],
                SlackEventParser.sanitise_user_input(result[TEXT_INDEX])
            )
            blocks.append(section_block_builder(section_text))
            blocks.append(divider_block_build())

        if Pagination.pagination_required(total_rows, page):
            blocks.append(Pagination.create_pagination_section(query_string, page + 1))
    else:
        blocks.append(section_block_builder("No design decisions matching the search criteria were found."))

    logging.info('... search result block building complete')
    return blocks
