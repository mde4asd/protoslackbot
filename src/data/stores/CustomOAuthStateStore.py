import logging
import time
from datetime import datetime  # type: ignore
from logging import Logger
from uuid import uuid4

from slack_sdk.oauth.state_store import OAuthStateStore
from sqlalchemy.engine import Engine
from sqlalchemy.orm import Session
from data.entities import Entities
from sqlalchemy.sql import select, delete


class CustomOAuthStateStore(OAuthStateStore):
    """
    A custom implementation of the OAuthStateStore used by Slack Bolt for Python.
    There is a need for a custom implementation so that the database operations can be implemented as
    required by our schema.
    """

    expiration_seconds: int
    engine: Engine

    def __init__(
            self,
            expiration_seconds: int,
            engine: Engine,
            logger: Logger = logging.getLogger(__name__)
    ):
        self.expiration_seconds = expiration_seconds
        self._logger = logger
        self.engine = engine

    @property
    def logger(self) -> Logger:
        if self._logger is None:
            self._logger = logging.getLogger(__name__)
        return self._logger

    def issue(self) -> str:
        """
        Generates a state parameter. This parameter is used during OAuth authentication to avoid forgery attacks.
        The value is unique to an installation of the bot.
        :return: state, a string representing a UUID.
        """
        with Session(self.engine) as session:
            session.begin()
            try:
                state = str(uuid4())
                expire_at = datetime.utcfromtimestamp(time.time() + self.expiration_seconds)
                new_state = Entities.OAuthState(state=state, expire_at=expire_at)
                session.add(new_state)
            except Exception as e:
                session.rollback()
                self.logger.error(e)
            else:
                session.commit()
                return state

    def consume(self, state: str) -> bool:
        """
        This method checks whether the provided state parameter is valid (exists in the database and has not expired)
        and then consumes it if it is valid.
        :param state: a UUID used during OAuth to avoid forgery attacks.
        :return: boolean, representing whether the state parameter was consumed or not
        (depending on whether it is valid)
        """
        with Session(self.engine) as session:
            try:
                oauth_states_table = Entities.OAuthState.__table__
                query = select(oauth_states_table).where(oauth_states_table.c.state == state).where(
                    oauth_states_table.c.expire_at > datetime.utcnow())
                result = session.execute(query)
                for row in result:
                    self.logger.debug(f"Consumed query result: {row}")
                    session.execute(delete(oauth_states_table).where(oauth_states_table.c.id == row["id"]))
                    session.commit()
                    return True
                return False
            except Exception as e:
                self.logger.error(e)
                return False
