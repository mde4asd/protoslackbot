import logging
from logging import Logger

from sqlalchemy.engine import Engine
from sqlalchemy.orm import Session
from sqlalchemy.sql import select, update
from data.entities import Entities


class WikiConfigurationStore:
    """
    A store for wiki configurations of the teams using the Slackbot.
    """

    engine: Engine

    def __init__(
            self,
            engine,
            logger: Logger = logging.getLogger(__name__),
    ):
        self.engine = engine
        self._logger = logger

    @property
    def logger(self) -> Logger:
        if self._logger is None:
            self._logger = logging.getLogger(__name__)
        return self._logger

    def save_wiki_configuration(self, configuration):
        """
        Saves the wiki configuration to the database.
        :param configuration: a dictionary containing data for a wiki configuration.
        :return: None, as only preforming a Crud operation.
        """
        with Session(self.engine) as session:
            session.begin()
            try:
                existing_configuration = self.get_wiki_configuration(configuration["team_id"])
                if existing_configuration:
                    wiki_configuration_table = Entities.WikiConfiguration.__table__
                    query = update(
                        wiki_configuration_table
                    ) \
                        .where(
                        wiki_configuration_table.c.team_id == configuration["team_id"]
                    ) \
                        .values(
                        host_url=configuration["host_url"],
                        project_id=configuration["project_id"],
                        slug=configuration["slug"],
                        access_token=configuration["access_token"]
                    )
                    session.execute(query)
                else:
                    new_configuration = Entities.WikiConfiguration(
                        team_id=configuration["team_id"],
                        host_url=configuration["host_url"],
                        project_id=configuration["project_id"],
                        slug=configuration["slug"],
                        access_token=configuration["access_token"]
                    )
                    session.add(new_configuration)
            except Exception as e:
                session.rollback()
                self.logger.error(e)
            else:
                session.commit()

    def get_wiki_configuration(self, team_id):
        """
        Returns the wiki configuration for the given team.
        :param team_id: the id of the team who the wiki belongs to.
        :return: WikiConfiguration, information regarding the wiki for the given team.
        """
        with Session(self.engine) as session:
            try:
                wiki_configuration_table = Entities.WikiConfiguration.__table__
                query = select(wiki_configuration_table).where(wiki_configuration_table.c.team_id == team_id)
                result = session.execute(query).first()
                return result
            except Exception as e:
                self.logger.error(e)
