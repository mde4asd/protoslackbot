import logging
from logging import Logger
from typing import Optional

from sqlalchemy.engine import Engine
from sqlalchemy.sql import select, update
from sqlalchemy import and_, desc
from sqlalchemy.orm import Session
from data.entities import Entities

from slack_sdk.oauth.installation_store.installation_store import InstallationStore
from slack_sdk.oauth.installation_store.models.bot import Bot
from slack_sdk.oauth.installation_store.models.installation import Installation

from data.entities import InstallationParser


class CustomInstallationStore(InstallationStore):
    """
    A custom implementation of the Installation Store used by Slack Bolt for Python.
    There is a need for a custom implementation so that the database operations can be implemented as
    required by our schema. Since we are only using bot scope installations the only methods needed to be
    implemented are:
        * save(installation)
        * find_bot(enterprise_id, team_id, is_enterprise_install)
    """

    client_id: str
    engine: Engine

    def __init__(
            self,
            client_id: str,
            engine: Engine,
            logger: Logger = logging.getLogger(__name__),
    ):
        self.client_id = client_id
        self._logger = logger
        self.engine = engine

    @property
    def logger(self) -> Logger:
        return self._logger

    def save(self, installation: Installation):
        """
        A required method that needs to be implemented. Since we are only using bot scope installations we pass
        the installation as a bot to save_bot(bot) which saves the installation to a database.
        :param installation: an object that contains information regarding the installation of the bot.
        :return: None
        """
        bot = installation.to_bot()
        self.save_bot(bot)

    def save_bot(self, bot: Bot):
        """
        Saves the data of the Slackbot installation. Checks to see whether an installation for the bot already exists.
        If an existing installation exists then it updates the installation, otherwise it creates a new installation.
        :param bot: an object that contains information regarding the installation of the bot.
        :return None
        """
        with Session(self.engine) as session:
            session.begin()
            try:
                b = bot.to_dict()
                b["client_id"] = self.client_id
                installations_table = Entities.Installation.__table__
                bots_rows = session.execute(
                    select(installations_table.c.id)
                    .where(
                        and_(
                            installations_table.c.client_id == self.client_id,
                            installations_table.c.enterprise_id == bot.enterprise_id,
                            installations_table.c.team_id == bot.team_id,
                            installations_table.c.installed_at == b.get("installed_at")
                        )
                    ).limit(1)
                )
                bots_row_id: Optional[str] = None
                for row in bots_rows:
                    bots_row_id = row["id"]
                if bots_row_id is None:
                    new_installation = InstallationParser.parse_installation(b)
                    session.add(new_installation)
                else:
                    update_statement = update.where(installations_table.c.id == bots_row_id).values(**b)
                    session.execute(update_statement, b)
            except Exception as e:
                session.rollback()
                self.logger.error(e)
            else:
                session.commit()

    def find_bot(
            self,
            *,
            enterprise_id: Optional[str],
            team_id: Optional[str],
            is_enterprise_install: Optional[bool] = False,
    ) -> Optional[Bot]:
        """
        Queries the database for a bot installation. The data from the bot installation is used to determine whether
        the information coming from an external source (bot) is valid.
        :param enterprise_id: the id of enterprise that installed the bot.
        :param team_id: the id of the team that installed the bot.
        :param is_enterprise_install: a boolean representing whether the Slackbot was installed by an enterprise or not.
        :return: Bot (if it exists). A bot contains information regarding its installation.
        """
        if is_enterprise_install or team_id is None:
            team_id = None

        installations_table = Entities.Installation.__table__
        query = (
            select(installations_table)
            .where(
                and_(
                    installations_table.c.client_id == self.client_id,
                    installations_table.c.enterprise_id == enterprise_id,
                    installations_table.c.team_id == team_id
                )
            )
            .order_by(desc(installations_table.c.installed_at))
            .limit(1)
        )

        with Session(self.engine) as session:
            result: object = session.execute(query)
            for row in result:
                return Bot(
                    app_id=row["app_id"],
                    enterprise_id=row["enterprise_id"],
                    enterprise_name=row["enterprise_name"],
                    team_id=row["team_id"],
                    team_name=row["team_name"],
                    bot_token=row["bot_token"],
                    bot_id=row["bot_id"],
                    bot_user_id=row["bot_user_id"],
                    bot_scopes=row["bot_scopes"],
                    bot_refresh_token=row["bot_refresh_token"],
                    bot_token_expires_at=row["bot_token_expires_at"],
                    is_enterprise_install=row["is_enterprise_install"],
                    installed_at=row["installed_at"],
                )
            return None

    def find_installation(
            self,
            *,
            enterprise_id: Optional[str],
            team_id: Optional[str],
            user_id: Optional[str] = None,
            is_enterprise_install: Optional[bool] = False,
    ) -> Optional[Installation]:
        """Finds a relevant installation for the given IDs.
        If the user_id is absent, this method may return the latest installation in the workspace / org.
        """
        raise NotImplementedError()

    def delete_bot(
            self,
            *,
            enterprise_id: Optional[str],
            team_id: Optional[str],
    ) -> None:
        """Deletes a bot scope installation per workspace / org"""
        raise NotImplementedError()

    def delete_installation(
            self,
            *,
            enterprise_id: Optional[str],
            team_id: Optional[str],
            user_id: Optional[str] = None,
    ) -> None:
        """Deletes an installation that matches the given IDs"""
        raise NotImplementedError()











