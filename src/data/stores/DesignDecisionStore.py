from sqlalchemy.orm import Session
from sqlalchemy.sql import select, delete, update, func
from sqlalchemy.engine import Engine
from sqlalchemy import and_, desc

from data.entities import Entities
from data.entities import DesignDecisionParser

import logging
from logging import Logger

from util import Pagination
from data.stores.UserStore import UserStore


class DesignDecisionStore:
    """
    A store for design decisions. Contains the methods that perform CRUD operations for design decisions.
    """

    engine: Engine
    user_store: UserStore

    def __init__(
            self,
            engine,
            user_store,
            logger: Logger = logging.getLogger(__name__)
    ):
        self.engine = engine
        self.user_store = user_store
        self._logger = logger

    @property
    def logger(self) -> Logger:
        if self._logger is None:
            self._logger = logging.getLogger(__name__)
        return self._logger

    def create_design_decision(self, data, user):
        """
        This method persists a design decision and its associated metacontent in a database
        :param data: The data from Slack that represents a recorded design decision
        :param user: The user who recorded the design decision
        :return: int: the id of the newly created design decision.
        """
        with Session(self.engine) as session:
            session.begin()
            try:
                new_design_decision = DesignDecisionParser.parse_design_decision(data)
                session.add(new_design_decision)
                session.flush()
                new_metacontent = DesignDecisionParser.parse_metacontent(data, new_design_decision.id)
                session.add(new_metacontent)
                session.flush()
                self.user_store.create_user(user, new_metacontent.id, session)
            except Exception as e:
                session.rollback()
                self.logger.error(e)
                raise
            else:
                session.commit()
                return new_design_decision.id

    def get_design_decision_id_by_message_id(self, message_id):
        """
        This method performs a query to get the id of the persisted design decision associated with the Slack message
        that represents the design decision
        :param message_id: The id of the Slack message
        :return: the id of the design decision
        """
        with Session(self.engine) as session:
            try:
                metacontent_table = Entities.MetaContent.__table__
                query = select(metacontent_table.c.decision_id).where(metacontent_table.c.message_id == message_id)
                decision_id = session.execute(query).first()[0]
                return decision_id
            except Exception as e:
                self.logger.error(e)

    def create_design_decision_component(self, sentence, label, message_id):
        """
        This method persists a design decision component based on its classification.
        :param sentence: The sentence which represents the design decision component
        :param label: The classification of the design decision component as selected by the user in Slack
        :param message_id: The id of the Slack message containing the design decision component
        :return: decision_id, design_decision_component_id the id's of the design decision component and the related
        design decision.
        """
        with Session(self.engine) as session:
            session.begin()
            try:
                decision_id = self.get_design_decision_id_by_message_id(message_id)
                new_design_decision_component = DesignDecisionParser.parse_design_decision_component(sentence, label,
                                                                                                     decision_id)
                session.add(new_design_decision_component)
            except Exception as e:
                session.rollback()
                self.logger.error(e)
            else:
                session.commit()
                return decision_id, new_design_decision_component.id

    def delete_design_decision_component_by_content(self, sentence, message_id):
        """
        When a user in Slack changes the classification of a design decision component (i.e. deselects a checkbox in
        Slack) the existing persisted design decision component needs to be deleted from the database. Since, the type
        of the component is unknown when deselected we have to "loop" through the component tables until the design
        decision component is successfully deleted.
        :param sentence: the sentence representing the design decision component.
        :param message_id: The id of the Slack message containing the design decision component
        :return: decision_id, label the id of the affected design decision and the type of the deleted
        component.
        """
        with Session(self.engine) as session:
            session.begin()
            try:
                decision_id = self.get_design_decision_id_by_message_id(message_id)
                component_deletion_queries = get_component_deletion_queries(sentence, decision_id)
                label = None
                for component_deletion_query in component_deletion_queries:
                    deleted = execute_component_deletion_query(component_deletion_query, session)
                    if deleted:
                        self.logger.info("Deleted design decision component by content.")
                        label = generate_label(component_deletion_query)
                        break
            except Exception as e:
                session.rollback()
                self.logger.error(e)
            else:
                session.commit()
                return decision_id, label

    def get_design_decision_by_text(self, text):
        """
        Returns the design decision with the given text field
        :param text: the textual content of the design decision.
        :return: DesignDecision, the design decision entity containing the given text
        """
        with Session(self.engine) as session:
            try:
                design_decision_table = Entities.DesignDecision.__table__
                query = select(design_decision_table).where(design_decision_table.c.text == text)
                result = session.execute(query).first()
                return result
            except Exception as e:
                self.logger.error(e)

    def get_metacontent_by_message_id_team_id_and_channel_id(self, message_id, team_id, channel_id):
        """
        Returns the metacontent for a design decision with the given team_id and channel_id
        :param message_id: the id of the message that contained the design decision
        :param team_id: the id of the team
        :param channel_id: the id of the channel the design decision occurred in
        :return: MetaContent, the metacontent entity containing the given message_id, team_id and channel_id
        """
        with Session(self.engine) as session:
            try:
                metacontent_table = Entities.MetaContent.__table__
                query = select(metacontent_table).where(
                    and_(
                        metacontent_table.c.message_id == message_id,
                        metacontent_table.c.team_id == team_id,
                        metacontent_table.c.channel_id == channel_id
                    )
                )
                result = session.execute(query).first()
                return result
            except Exception as e:
                self.logger.error(e)

    def search_design_decisions(self, team_id, query_string, page=0):
        """
        This method retrieves the first 5 design decisions for a team matching the search criteria
        :param team_id: the id of the Slack team's workspace that performed the search
        :param query_string: a string containing the search criteria
        :param page: the current page number of results (design decisions)
        :return: results, a list of design decisions matching the search criteria.
        """
        with Session(self.engine) as session:
            try:
                metacontent_table = Entities.MetaContent.__table__
                decisions_table = Entities.DesignDecision.__table__
                metacontent_user_table = Entities.MetaContentUser.__table__
                users_table = Entities.User.__table__
                tables = [decisions_table, metacontent_table, metacontent_user_table, users_table]

                like_string = "%{}%".format(query_string)
                filters = [
                    decisions_table.c.id == metacontent_table.c.decision_id,
                    metacontent_table.c.id == metacontent_user_table.c.metacontent_id,
                    users_table.c.id == metacontent_user_table.c.user_id,
                    metacontent_table.c.team_id == team_id,
                    decisions_table.c.text.like(like_string)
                ]

                total_rows = session.query(
                    *tables
                ).filter(
                    *filters
                ).order_by(
                    desc(metacontent_table.c.date_time)
                ).count()

                results = session.query(
                    *tables
                ).filter(
                    *filters
                ).order_by(
                    desc(metacontent_table.c.date_time)
                ).offset(page * Pagination.PAGE_SIZE).limit(Pagination.PAGE_SIZE).all()

                return results, total_rows
            except Exception as e:
                self.logger.error(e)

    def amend_design_decision(self, decision_id, design_decision, user, message_id):
        """
        Performs the database operations needed to update a design decision.
        1) Updates the design decision.
        2) Updates the metacontent of the design decision.
        3) Updates the user - metacontent association if the user who updated the design decision differs from
        the original design decision recorder.
        4) Deletes all the classified design decision components (a current simple solution, that may want to be
        changed in the future)
        :param decision_id: the id of the design decision to be updated
        :param design_decision: the textual representation of the design decision to be updated
        :param user: the user who updated the design decision
        :param message_id: the id of the message where the design decision has been amended
        :return: None, as only database transaction is performed.
        """
        with Session(self.engine) as session:
            session.begin()
            try:
                self.update_design_decision(decision_id, design_decision, session)
                self.update_metacontent(decision_id, message_id, session)
                self.update_metacontent_user_association(decision_id, user, session)
                self.delete_design_decisions_components_by_decision_id(decision_id, session)
            except Exception as e:
                session.rollback()
                self.logger.error(e)
            else:
                session.commit()

    def update_design_decision(self, decision_id, design_decision, session):
        self.logger.debug("... updating design decision - DesignDecision")
        design_decision_table = Entities.DesignDecision.__table__
        query = update(
            design_decision_table
        ) \
            .where(
            design_decision_table.c.id == decision_id
        ) \
            .values(
            text=design_decision
        )
        session.execute(query)

    def update_metacontent(self, decision_id, message_id, session):
        self.logger.debug("... updating design decision - MetaContent")
        metacontent_table = Entities.MetaContent.__table__
        query = update(
            metacontent_table
        ) \
            .where(
            metacontent_table.c.decision_id == decision_id
        ) \
            .values(
            message_id=message_id,
            date_time=func.now()
        )
        session.execute(query)

    def update_metacontent_user_association(self, decision_id, user, session):
        """
        This method updates the association between a design decisions metacontent and design decisions recorder.
        If the original design decision recorder and current user are different, than the association is updated
        :param decision_id: the id of the design decision
        :param user: the user who updated the design decision
        :param session: An open database session that allows us to interact with the database
        :return: None, as only part of a database transaction is performed.
        """
        self.logger.debug("... updating design decision - MetaContentUser")
        metacontent_id = self.get_metacontent_id_by_decision_id(decision_id)
        original_recorder_id = self.user_store.get_user_id_by_metacontent_id(metacontent_id)
        original_recorder = self.user_store.get_user_by_id(original_recorder_id)
        update_by_user = self.user_store.get_user_by_slack_id_and_name(user["slack_id"], user["name"])
        if original_recorder and update_by_user and (original_recorder["name"] != update_by_user["name"]):
            self.delete_metacontent_user_association(metacontent_id, original_recorder_id, session)
            self.user_store.create_user(user, metacontent_id, session)

    def delete_metacontent_user_association(self, metacontent_id, user_id, session):
        self.logger.debug("... deleting MetaContent - User association")
        metacontent_user_association_table = Entities.MetaContentUser.__table__
        query = delete(
            metacontent_user_association_table
        ).where(
            and_(
                metacontent_user_association_table.c.metacontent_id == metacontent_id,
                metacontent_user_association_table.c.user_id == user_id
            )
        )
        session.execute(query)

    def get_metacontent_id_by_decision_id(self, decision_id):
        with Session(self.engine) as session:
            try:
                metacontent_table = Entities.MetaContent.__table__
                query = select(metacontent_table.c.id).where(metacontent_table.c.decision_id == decision_id)
                metacontent_id = session.execute(query).first()[0]
                return metacontent_id
            except Exception as e:
                self.logger.error(e)

    def delete_design_decisions_components_by_decision_id(self, decision_id, session):
        component_deletion_queries = get_component_deletion_queries("", decision_id)
        for component_deletion_query in component_deletion_queries:
            execute_component_deletion_query(component_deletion_query, session)
            self.logger.debug("Deleted design decision component by decision id.")

    def get_design_decision_by_id(self, decision_id):
        with Session(self.engine) as session:
            try:
                design_decision_table = Entities.DesignDecision.__table__
                query = select(design_decision_table).where(design_decision_table.c.id == decision_id)
                result = session.execute(query).first()
                return result
            except Exception as e:
                self.logger.error(e)


def execute_component_deletion_query(query, session):
    result = session.execute(query)
    return result.rowcount > 0


def get_component_deletion_queries(sentence, decision_id):
    return [
        generate_delete_context_query(sentence, decision_id),
        generate_delete_non_functional_concern_query(sentence, decision_id),
        generate_delete_options_query(sentence, decision_id),
        generate_delete_quality_attribute_query(sentence, decision_id),
        generate_delete_consequences_query_query(sentence, decision_id)
    ]


def generate_delete_context_query(context, decision_id):
    context_table = Entities.Context.__table__
    like_context = "%{}%".format(context)
    return delete(
        context_table
    ).where(
        and_(
            context_table.c.decision_id == decision_id,
            context_table.c.context.like(like_context)
        )
    )


def generate_delete_non_functional_concern_query(concern, decision_id):
    non_functional_concern_table = Entities.NonFunctionalConcern.__table__
    like_concern = "%{}%".format(concern)
    return delete(
        non_functional_concern_table
    ).where(
        and_(
            non_functional_concern_table.c.decision_id == decision_id,
            non_functional_concern_table.c.concern.like(like_concern)
        )
    )


def generate_delete_options_query(options, decision_id):
    options_table = Entities.Options.__table__
    like_options = "%{}%".format(options)
    return delete(
        options_table
    ).where(
        and_(
            options_table.c.decision_id == decision_id,
            options_table.c.options.like(like_options)
        )
    )


def generate_delete_quality_attribute_query(name, decision_id):
    quality_attribute_table = Entities.QualityAttribute.__table__
    like_name = "%{}%".format(name)
    return delete(
        quality_attribute_table
    ).where(
        and_(
            quality_attribute_table.c.decision_id == decision_id,
            quality_attribute_table.c.name.like(like_name)
        )
    )


def generate_delete_consequences_query_query(consequence, decision_id):
    consequence_table = Entities.Consequence.__table__
    like_consequence = "%{}%".format(consequence)
    return delete(
        consequence_table
    ).where(
        and_(
            consequence_table.c.decision_id == decision_id,
            consequence_table.c.consequence.like(like_consequence)
        )
    )


def generate_label(query):
    """
    Generates a candidate label based on a database table name, since they don't map directly
    :param query: the deletion query that was run containing the table name
    :return: a candidate label for the deleted component.
    """
    entity = str(query).split(" ")[2]
    match entity:
        case "context":
            entity = "use_case"
        case "non_functional_concerns":
            entity = "non_functional_concern"
        case "options":
            entity = "option"
    return entity.upper().replace("_", " ") + ": DELETED"
