import logging
from logging import Logger

from sqlalchemy.engine import Engine
from sqlalchemy.orm import Session
from sqlalchemy.sql import select
from sqlalchemy import and_

from data.entities import Entities
from data.entities import DesignDecisionParser


class UserStore:
    """
    A store for users who record design decisions in Slack.
    """

    engine: Engine

    def __init__(
            self,
            engine,
            logger: Logger = logging.getLogger(__name__),
    ):
        self.engine = engine
        self._logger = logger

    @property
    def logger(self) -> Logger:
        if self._logger is None:
            self._logger = logging.getLogger(__name__)
        return self._logger

    def create_user(self, user, metacontent_id, session):
        """
        This method creates a new user if they do not already exist. An association between the
        newly created user (or existing user) and metacontent is then made
        :param user: a dictionary containing the user's Slack id and name
        :param metacontent_id: the id of the corresponding metacontent
        :param session: An open session that allows us to converse with the database to perform CRUD operations
        :return: None, as the method only performs a database transaction.
        """
        user_id = self.get_user_id_by_slack_id(user["slack_id"])
        if user_id is None:
            new_user = DesignDecisionParser.parse_user(user)
            session.add(new_user)
            session.flush()
            user_id = new_user.id
        new_association = DesignDecisionParser.parse_metacontent_user_association(user_id, metacontent_id)
        session.add(new_association)

    def get_user_id_by_slack_id(self, slack_id):
        with Session(self.engine) as session:
            try:
                user_table = Entities.User.__table__
                query = select(user_table.c.id).where(user_table.c.slack_id == slack_id)
                user_id = session.execute(query).first()[0]
                return user_id
            except Exception as e:
                self.logger.error(e)

    def get_user_id_by_metacontent_id(self, metacontent_id):
        with Session(self.engine) as session:
            try:
                metacontent_user_table = Entities.MetaContentUser.__table__
                query = select(
                    metacontent_user_table.c.user_id
                ).where(
                    metacontent_user_table.c.metacontent_id == metacontent_id
                )
                user_id = session.execute(query).first()[0]
                return user_id
            except Exception as e:
                self.logger.error(e)

    def get_user_by_slack_id_and_name(self, slack_id, name):
        with Session(self.engine) as session:
            try:
                users_table = Entities.User.__table__
                query = select(users_table).where(
                    and_(
                        users_table.c.slack_id == slack_id,
                        users_table.c.name == name
                    )
                )
                result = session.execute(query).first()
                return result
            except Exception as e:
                self.logger.error(e)

    def get_user_by_id(self, user_id):
        with Session(self.engine) as session:
            try:
                user_table = Entities.User.__table__
                query = select(user_table).where(user_table.c.id == user_id)
                user = session.execute(query).first()
                return user
            except Exception as e:
                self.logger.error(e)
