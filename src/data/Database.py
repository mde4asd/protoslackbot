import os
from dotenv import load_dotenv
from sqlalchemy import create_engine

from data.entities import Entities
from model.Environment import Environment

import logging
logger = logging.getLogger(__name__)

load_dotenv()


def setup_db(connection_str="sqlite:///slackbot.db", environment=os.environ.get("ENVIRONMENT")):
    """
    Sets up the database required to store design decision captured in Slack.

    If the Slackbot is deployed to production it will use a MariaDB database.
    Otherwise it will use an in SQLite for development and testing purposes.

    Once the database type is defined all tables used to model the design decisions are created.
    :return: engine, the database.
    """
    if environment == Environment.PROD.name:
        engine = create_engine(
            "mariadb+mariadbconnector://{}:{}@127.0.0.1:3306/slackbot".format(
                os.environ.get("DB_USERNAME"),
                os.environ.get("DB_PASSWORD")
            ),
            pool_pre_ping=True
        )
    else:
        engine = create_engine(connection_str, echo=True, future=True)
    Entities.Base.metadata.create_all(engine)
    return engine
