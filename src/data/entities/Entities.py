from sqlalchemy.orm import declarative_base, relationship
from sqlalchemy.sql import func
from sqlalchemy import Column, Integer, String, DateTime, ForeignKey, Boolean
import sqlalchemy

DESIGN_DECISION_FOREIGN_KEY = 'design_decisions.id'

Base = declarative_base()


class MetaContentUser(Base):
    __tablename__ = "metacontent_user"

    metacontent_id = Column(Integer, ForeignKey("metacontent.id"), primary_key=True)
    user_id = Column(Integer, ForeignKey("users.id"), primary_key=True)

    def __repr__(self):
        return "<MetaContentUser(metacontent_id='%s')>" % self.metacontent_id


class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, autoincrement=True)
    slack_id = Column(String(32))
    name = Column(String(255))

    def __repr__(self):
        return "<User(name='%s')>" % self.name


class MetaContent(Base):
    __tablename__ = "metacontent"

    id = Column(Integer, primary_key=True, autoincrement=True)
    date_time = Column(DateTime(timezone=True), server_default=func.now())
    message_id = Column(String(32))
    team_id = Column(String(32))
    channel_id = Column(String(32))

    # One set of metacontent to one Design Decision
    decision_id = Column(Integer, ForeignKey(DESIGN_DECISION_FOREIGN_KEY))
    decision = relationship("DesignDecision", back_populates="metacontent")

    user: User = relationship("User", secondary="metacontent_user")

    def __repr__(self):
        return "<MetaContent(message_id='%s')>" % self.message_id


class Context(Base):
    __tablename__ = "context"

    id = Column(Integer, primary_key=True, autoincrement=True)
    context = Column(String(255))

    decision_id = Column(Integer, ForeignKey(DESIGN_DECISION_FOREIGN_KEY))
    decision = relationship("DesignDecision", back_populates="context")

    def __repr__(self):
        return "<Context(context='%s')>" % self.context


class NonFunctionalConcern(Base):
    __tablename__ = "non_functional_concerns"

    id = Column(Integer, primary_key=True, autoincrement=True)
    concern = Column(String(255))

    decision_id = Column(Integer, ForeignKey(DESIGN_DECISION_FOREIGN_KEY))
    decision = relationship("DesignDecision", back_populates="nonFunctionalConcern")

    def __repr__(self):
        return "<NonFunctionalConcern(concern='%s')>" % self.concern


class Consequence(Base):
    __tablename__ = "consequences"

    id = Column(Integer, primary_key=True, autoincrement=True)
    consequence = Column(String(255))

    decision_id = Column(Integer, ForeignKey(DESIGN_DECISION_FOREIGN_KEY))
    decision = relationship("DesignDecision", back_populates="consequence")

    def __repr__(self):
        return "<Consequence(consequence='%s')>" % self.consequence


class QualityAttribute(Base):
    __tablename__ = "quality_attributes"

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(255))

    decision_id = Column(Integer, ForeignKey(DESIGN_DECISION_FOREIGN_KEY))
    decision = relationship("DesignDecision", back_populates="qualityAttribute")

    def __repr__(self):
        return "<QualityAttribute(name='%s')>" % self.name


class Options(Base):
    __tablename__ = "options"

    id = Column(Integer, primary_key=True, autoincrement=True)
    options = Column(String(255))

    decision_id = Column(Integer, ForeignKey(DESIGN_DECISION_FOREIGN_KEY))
    decision = relationship("DesignDecision", back_populates="options")

    def __repr__(self):
        return "<Options(options='%s')>" % self.options


class DesignDecision(Base):
    __tablename__ = "design_decisions"

    id = Column(Integer, primary_key=True, autoincrement=True)
    text = Column(String(1500))

    metacontent: MetaContent = relationship("MetaContent", back_populates="decision", uselist=False)
    context: Context = relationship("Context", back_populates="decision", uselist=False)
    nonFunctionalConcern: NonFunctionalConcern = relationship("NonFunctionalConcern", back_populates="decision",
                                                              uselist=False)
    options: Options = relationship("Options", back_populates="decision", uselist=False)
    consequence: Consequence = relationship("Consequence", back_populates="decision", uselist=False)
    qualityAttribute: QualityAttribute = relationship("QualityAttribute", back_populates="decision", uselist=False)

    def __repr__(self):
        return "<DesignDecision(text='%s')>" % self.text


class OAuthState(Base):
    __tablename__ = "oauth_states"

    id = Column(Integer, primary_key=True, autoincrement=True)
    state = Column(String(200), nullable=False)
    expire_at = Column(DateTime, nullable=False)

    def __repr__(self):
        return "<OAuthState(state='%s')>" % self.state


class Installation(Base):
    __tablename__ = "installations"

    id = Column(Integer, primary_key=True, autoincrement=True)
    client_id = Column(String(32), nullable=False)
    app_id = Column(String(32), nullable=False)
    enterprise_id = Column(String(32))
    enterprise_name = Column(String(200))
    team_id = Column(String(32))
    team_name = Column(String(200))
    bot_token = Column(String(255))
    bot_id = Column(String(32))
    bot_user_id = Column(String(32))
    bot_scopes = Column(String(1000))
    bot_refresh_token = Column(String(200))
    bot_token_expires_at = Column(DateTime)
    is_enterprise_install = Column(Boolean, default=False, nullable=False)
    installed_at = Column(DateTime, nullable=False, default=sqlalchemy.sql.func.now())

    def __repr__(self):
        return "<Installation(app_id='%s')>" % self.app_id


class WikiConfiguration(Base):
    __tablename__ = "wiki_configurations"

    id = Column(Integer, primary_key=True, autoincrement=True)
    team_id = Column(String(32))
    host_url = Column(String(255))
    project_id = Column(Integer)
    slug = Column(String(255))
    access_token = Column(String(255))

    def __repr__(self):
        return "<WikiConfiguration(host_url='%s')>" % self.host_url


def main():
    pass


if __name__ == "__main__":
    main()
