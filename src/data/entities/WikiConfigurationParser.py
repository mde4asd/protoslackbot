
def parse_configuration(command):
    command_inputs = command["text"].split(",")
    host_url = command_inputs[0].strip()
    project_id = int(command_inputs[1].strip())
    slug = command_inputs[2].strip()
    access_token = command_inputs[3].strip()
    return {
        "team_id": command["team_id"],
        "host_url": host_url,
        "project_id": project_id,
        "slug": slug,
        "access_token": access_token
    }
