from data.entities import Entities


def parse_installation(data):
    return Entities.Installation(
        client_id=data["client_id"],
        app_id=data["app_id"],
        enterprise_id=data["enterprise_id"],
        enterprise_name=data["enterprise_name"],
        team_id=data["team_id"],
        team_name=data["team_name"],
        bot_token=data["bot_token"],
        bot_id=data["bot_id"],
        bot_user_id=data["bot_user_id"],
        bot_scopes=data["bot_scopes"],
        bot_refresh_token=data["bot_refresh_token"],
        bot_token_expires_at=data["bot_token_expires_at"],
        is_enterprise_install=data["is_enterprise_install"],
        installed_at=data["installed_at"]
    )
