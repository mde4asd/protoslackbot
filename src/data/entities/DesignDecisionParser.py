from data.entities import Entities
from model.Candidate import Candidate
from util import SlackEventParser
import logging

logger = logging.getLogger(__name__)


def parse_metacontent(data, decision_id):
    message_id = data['ts']
    team_id = data['team']
    channel_id = data['channel']
    return Entities.MetaContent(
        message_id=message_id,
        team_id=team_id,
        channel_id=channel_id,
        decision_id=decision_id
    )


def parse_design_decision(data):
    text = SlackEventParser.sanitise_user_input(data["text"])
    return Entities.DesignDecision(
        text=text
    )


def parse_user(user):
    return Entities.User(
        slack_id=user["slack_id"],
        name=user["name"],
    )


def parse_metacontent_user_association(user_id, metacontent_id):
    return Entities.MetaContentUser(
        user_id=user_id,
        metacontent_id=metacontent_id
    )


def parse_design_decision_component(sentence, label, decision_id):
    """
    This method returns an SQLAlchemy entity representing the design decision component so that it can be persisted
    in a database.
    :param sentence: The sentence which represents the design decision component.
    :param label: The classification of the design decision component as selected by the user in Slack.
    :param decision_id: the id of the associated design decision
    :return: an SQLAlchemy entity representing the design decision component.
    """
    try:
        type_component = "_".join(label.split(":")[0].upper().split(" "))
        match Candidate[type_component]:
            case Candidate.USE_CASE | Candidate.COMPONENT:
                return parse_context(sentence, decision_id)
            case Candidate.NON_FUNCTIONAL_CONCERN:
                return parse_non_functional_concern(sentence, decision_id)
            case Candidate.OPTION | Candidate.OTHER_OPTIONS:
                return parse_options(sentence, decision_id)
            case Candidate.QUALITY_ATTRIBUTES:
                return parse_quality_attribute(sentence, decision_id)
            case Candidate.CONSEQUENCES:
                return parse_consequences(sentence, decision_id)
            case _:
                raise ValueError("Not a valid design decision component type.")
    except KeyError as e:
        logger.error(e)
        raise ValueError("Not a valid design decision component type.") from None


def parse_context(context, decision_id):
    return Entities.Context(
        context=context,
        decision_id=decision_id
    )


def parse_non_functional_concern(concern, decision_id):
    return Entities.NonFunctionalConcern(
        concern=concern,
        decision_id=decision_id
    )


def parse_options(options, decision_id):
    return Entities.Options(
        options=options,
        decision_id=decision_id
    )


def parse_quality_attribute(quality_attribute,  decision_id):
    return Entities.QualityAttribute(
        name=quality_attribute,
        decision_id=decision_id
    )


def parse_consequences(consequence, decision_id):
    return Entities.Consequence(
        consequence=consequence,
        decision_id=decision_id
    )


def parse_amend_command(command):
    command_inputs = command["text"].split("|")
    decision_id = int(command_inputs[0])
    design_decision = command_inputs[1].strip()
    return decision_id, design_decision
