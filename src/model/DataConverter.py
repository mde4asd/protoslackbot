"""
Labelled data from Gamify.py comes as a csv file type.
To fine tune the model an indexed true format is required
A Josephs
August 2021
"""

import csv
from src.model import Candidate


def convert_values(filename, c_labels):
    """
    Take in a file with labelled data and return representation
    of the labels in columns
    example:
    one hot representation/
    'is there a better way?,NA' >> `is there a better way?,0,0,0,0,0,0,0,0,1`
    Where N/A is represented by the enum value [9]
    :param filename: labeled data filename
    :param c_labels: labels as list from enum Candidate.py
    :return: classifications as an array with 0|1 to represent label
    """
    out = []
    with open(filename, newline='\n') as csv_file:
        data_reader = csv.reader(csv_file, delimiter=',')
        headers = ['SENTENCE'] + list(c_labels.keys())
        out.append(headers)
        for row in data_reader:
            new_row = [0 for i in range(0, len(c_labels) + 1)]
            new_row[0] = row[0]
            for label in row[1:]:
                new_row[c_labels[label]] = 1
            out.append(new_row)
        pretty_print_array(out, c_labels)
    return out


def convert_first_value(filepath, c_labels):
    """
    Take in a file with labelled data and return representation
    of the labels in columns
    example:
    'is there a better way?,NA' >> `is there a better way?,0,0,0,0,0,0,0,0,1`
    Where N/A is represented by the enum value [9]
    :param filename: labeled data filename
    :param c_labels: labels as list from enum Candidate.py
    :return: classifications as an array with 0|1 to represent label
    """
    filename = f'{filepath}sample.csv'
    out = []
    with open(filename, newline='\n') as csv_file:
        data_reader = csv.reader(csv_file, delimiter=',')
        headers = ['SENTENCE'] + list(c_labels.keys())
        out.append(headers)
        for row in data_reader:
            new_row = [0 for i in range(0, len(c_labels) + 1)]
            new_row[0] = row[0]  # take the sentence as the row first element
            new_row[c_labels[row[1]]] = 1
            out.append(new_row)
        pretty_print_array(out, c_labels)
    return out


def write_converted(data, filepath):
    """
    Take representation of labels in 0|1 format
    and write to new csv file
    :param filename: filename used to save file out
    :param data:
    :return: None
    """
    filename = f'{filepath}c_1label'
    with open(filename, 'w', newline='\n') as csvOut:
        writer = csv.writer(csvOut, delimiter=',')
        for row in data:
            writer.writerow(row)


def pretty_print_array(data, labels):
    """"
    @Param _list: an array containing a sentence in first index and list of integer
     representations of true false
    values following. print the sentences with equal spacing before starting the integer
    values
    """
    print("\n\n")
    # print(f'{"Sentence":<100} {list(labels.keys())}')
    for row in data:
        if len(row[0]) > 100:
            print(f'{row[0][:100]:<100}')
            print(f'{row[0][100:]:<100} {row[1:]}')
        else:
            print(f'{row[0]:<100} {row[1:]}')


def main():
    filepath = '../../resources/data/'

    c_labels = Candidate.generate_dict()
    data = convert_first_value(filepath, c_labels)
    write_converted(data, filepath)
    print(c_labels)


if __name__ == '__main__':
    main()
