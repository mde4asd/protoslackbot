from enum import Enum


class Environment(Enum):
    PROD = "PROD"
    DEV = "DEV"
    TEST = "TEST"
