"""
Fine Tuned Version:
Uses Transformer and fine tuned dataset to classify sentences
A.Josephs 😀
October 2021 👀
"""
from transformers import AutoTokenizer
from collections import defaultdict
import logging

from model import ClauseFormer
from model.Candidate import Candidate
from model.WhyCommentTagger import WHYCommentTagger

logging.basicConfig(level=logging.DEBUG)


class Classifier:
    def __init__(self):
        self.checkpoint = 'bert-base-cased'
        self.candidate_labels = Candidate.generate_list()
        self.tokenizer = AutoTokenizer.from_pretrained(self.checkpoint)
        self.model = WHYCommentTagger.load_from_checkpoint("model/best-checkpoint.ckpt",
                                                           n_classes=len(self.candidate_labels))
        logging.info("...build classifier model")
        self.model.eval()
        self.model.freeze()

    def classify_user_input(self, user_text):
        """
        Here we receive the full paragraph message from the user to
        be separated into single subject clauses by spaCy with each single subject clause
        then classified and added to the class dictionary for return to user
        :param user_text: design decision message from user
        :return: list of classified single subject clauses cumulative for WH(Y) model type
        """
        classified_clause = defaultdict(list)
        logging.info("...split single subject clauses")
        clauses_list = ClauseFormer.split_into_single_subject_clauses(user_text)

        for clause in clauses_list:
            top_three_labels = self.classify_text(clause)
            classified_clause[clause].append(top_three_labels)

        logging.info("...return results")
        return classified_clause

    def classify_text(self, clause):
        """
        Here we receive the text (as a clause) from the user that is to be
        classified.
        :param clause: single subject sentence
        :return: label for clause after classification
        """
        logging.info("...classify sentence")

        encoding = self.tokenizer.encode_plus(clause,
                                              add_special_tokens=True,
                                              max_length=128,
                                              return_token_type_ids=False,
                                              padding="max_length",
                                              return_attention_mask=True,
                                              return_tensors="pt")

        _, prediction = self.model(encoding["input_ids"], encoding["attention_mask"])
        prediction = prediction.flatten().numpy()
        top_3_results = self.define_lead_results(prediction)

        logging.info("...return classification")
        return top_3_results

    def define_lead_results(self, predictions):
        """
        Here, using the calculated response from the classifier we take the
        top [3] results
        :param predictions: list of probability for each label option
        :return: top three options with labels as list of string
        """
        top_options = []
        logging.debug(f'RESULTS: {predictions}')
        results = list(zip(self.candidate_labels, predictions))
        results.sort(key=lambda x: x[1], reverse=True)
        for label, probability in results:
            if label != 'UNKNOWN':
                top_options.append(f"{label}: {probability:.3f}")
        logging.debug(f'TOP THREE RESULTS: {top_options[:3]}')

        return top_options[:3]


def print_readable_results(results):
    """
    Use the results from the classifier and print them in a readable format
    :param results: results from classification
    """
    for i in range(len(results['labels'])):
        print("{}: {}".format(results['labels'][i], results['scores'][i]))


def main():
    ft_classifier = Classifier()
    labels = ft_classifier.classify_text("hello")
    print(labels)


if __name__ == "__main__":
    main()
