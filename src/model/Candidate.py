""" 
Candidate Labels for model classification
A Josephs
"""
from enum import Enum, unique, auto


@unique
class Candidate(Enum):
    """
    Candidates from WH(Y) classification scheme
    """
    USE_CASE = auto()
    COMPONENT = auto()
    NON_FUNCTIONAL_CONCERN = auto()
    OPTION = auto()
    OTHER_OPTIONS = auto()
    QUALITY_ATTRIBUTES = auto()
    CONSEQUENCES = auto()
    UNKNOWN = auto()
    NA = auto()

    def __str__(self):
        label = self.value.title()
        return label

    def generate_list():
        """
        Return a list of candidates from enum
        :return:
        """
        _list = []
        for label in Candidate:
            _list.append(label.name)
        return _list

    def generate_dict():
        """
        Return a dictionary of candidates from enum
        :return:
        """
        _dict = dict()
        for label in Candidate:
            _dict[label.name] = label.value
        return _dict


def generate_dict():
    c = Candidate.generate_dict()
    return c
