import spacy
# Used visualise how the tokens are labelled
from spacy import displacy


def split_into_single_subject_clauses(text):
    """
    Takes the text received from a user in Slack and splits it into sentences and single subject clauses.
    Compound and complex sentences need to be split into single subject clauses as their parts may be classified
    differently.
    :param text: message sent through Slack channel for classification
    :return: list of sentences and single subject clauses
    """
    try:
        nlp = spacy.load('en_core_web_sm')
        doc = nlp(text)
        clauses = []
        for sentence in doc.sents:
            last = sentence[0].i
            for token in sentence:
                if is_split_needed(token):
                    if is_comma(token):
                        clauses.append(doc[last:token.i + 1])
                        last = token.i + 1
                    else:
                        clauses.append(doc[last:token.i])
                        last = token.i
            clauses.append(doc[last:(sentence[-1].i + 1)])
            # Uncomment to visualise how the tokens are labelled
            # displacy.serve(sent, style="dep")
        return [clause.text for clause in clauses if len(clause.text) > 0]
    except Exception as e:
        print('ClauseFormer.split_into_single_subject_clauses:', e)
        print('ClauseFormer.split_into_single_subject_clauses: en_core_wb_sm may not be installed, see README for '
              'instructions')


def is_split_needed(token):
    """
    This function determines whether a sentence needs to be split into single subject clauses based on rules outlined
    in the Gitlab Wiki (found here: https://gitlab.com/mde4asd/protoslackbot/-/wikis/Decisions/Parsing%20Heuristics)
    If the current token has no neighbour i.e. it is the end of the sentence then an index error will be thrown and
    caught signifying the sentence does not need to be split
    :param token: the current token (word, punctuation, etc.) in the sentence.
    :return: a boolean value, True if sentence needs to be split, otherwise False
    """
    try:
        next_token = token.nbor()
        return (follows_subordinating_conjunction_rule(token, next_token)
                or follows_correlative_conjunction_rule(token, next_token)
                or follows_comma_rule(token, next_token)
                or follows_adverbial_clause_rule(token, next_token)
                or follows_non_functional_concern_rule(token, next_token)
                or follows_other_options_rule(token, next_token)
                or follows_quality_attribute_rule(token, next_token)
                or follows_consequences_rule(token, next_token))
    except IndexError as e:
        print('ClauseFormer.is_split_needed: No neighbouring token (end of sentence),', e)
        return False


def follows_subordinating_conjunction_rule(token, next_token):
    """
    A rule used to split sentences. If the current token is a subordinating conjunction (such as, although, because,
    and though) and followed by a verb or pronoun that is a nominal subject then the sentence needs to be split.
    :param token: the current token
    :param next_token: the token after token
    :return: a boolean value, True if token and next token follow the given rule, otherwise False
    """
    return is_subordinating_conjunction(token) and (is_verb(next_token) or is_nominal_pronoun(next_token))


def follows_correlative_conjunction_rule(token, next_token):
    """
    A rule used to split sentences. If the current token is a correlative conjunction (such as, and, but, and or)
    and followed by a verb or nominal subject then the sentence needs to be split.
    :param token: the current token
    :param next_token: the token after token
    :return: a boolean value, True if token and next token follow the given rule, otherwise False
    """
    return is_correlative_conjunction(token) and (is_verb(next_token) or is_nominal_subject(next_token))


def follows_comma_rule(token, next_token):
    """
    A rule used to split sentences. If the current token is a comma and followed by a nominal subject then the sentence
    needs to be split.
    :param token: the current token
    :param next_token: the token after token
    :return: a boolean value, True if token and next token follow the given rule, otherwise False
    """
    return is_comma(token) and is_nominal_subject(next_token)


def follows_adverbial_clause_rule(token, next_token):
    """
    A rule used to split sentences. If the current token is an adverb and followed by a nominal subject then the
    sentence needs to be split.
    :param token: the current token
    :param next_token: the token after token
    :return: a boolean value, True if token and next token follow the given rule, otherwise False
    """
    return is_adverb(token) and is_nominal_subject(next_token)


def follows_non_functional_concern_rule(token, next_token):
    """
    A rule used to split sentences. If the current token is a comma and followed by the facing keyword
    or the facing keyword is present (following the WH(Y) model) then the sentence needs to be split
    :param token: the current token
    :param next_token: the token after token
    :return: a boolean value, True if token and next token follow the given rule, otherwise False
    """
    return (is_comma(token) and is_facing(next_token)) or (is_facing(token))


def follows_other_options_rule(token, next_token):
    """
    A rule used to split sentences. If the current token is and, and followed by the neglected keyword
    (following the WH(Y) model) then the sentence needs to be split
    :param token: the current token
    :param next_token: the token after token
    :return: a boolean value, True if token and next token follow the given rule, otherwise False
    """
    return is_and(token) and is_neglected(next_token)


def follows_quality_attribute_rule(token, next_token):
    """
    A rule used to split sentences. If the current token is to, and followed by the achieve keyword
    (following the WH(Y) model) then the sentence needs to be split
    :param token: the current token
    :param next_token: the token after token
    :return: a boolean value, True if token and next token follow the given rule, otherwise False
    """
    return is_to(token) and is_achieve(next_token)


def follows_consequences_rule(token, next_token):
    """
    A rule used to split sentences. If the current token is the accepting keyword, and followed by the downside keyword
    or the accepting keyword is present (following the WH(Y) model) then the sentence needs to be split
    :param token: the current token
    :param next_token: the token after token
    :return: a boolean value, True if token and next token follow the given rule, otherwise False
    """
    return (is_accepting(token) and is_downside(next_token)) or (is_accepting(token))


def is_subordinating_conjunction(token):
    return token.pos_ == "SCONJ" and token.dep_ == "mark"


def is_verb(token):
    return token.pos_ == "VERB"


def is_comma(token):
    return token.text == ","


def is_nominal_pronoun(token):
    return token.pos_ == "PRON" and token.dep_ == "nsubj"


def is_correlative_conjunction(token):
    return token.pos_ == "CCONJ" and token.dep_ == "cc"


def is_nominal_subject(token):
    return token.dep_ == "nsubj"


def is_adverb(token):
    return token.pos_ == "ADV" and token.dep_ == "advmod"


def is_facing(token):
    return token.text == "facing"


def is_and(token):
    return token.text == "and"


def is_neglected(token):
    return token.text == "neglected"


def is_to(token):
    return token.text == "to"


def is_achieve(token):
    return token.text == "achieve"


def is_accepting(token):
    return token.text == "accepting"


def is_downside(token):
    return token.text == "downside"
