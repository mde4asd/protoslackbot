import unittest
from data import Database
from data.stores.WikiConfigurationStore import WikiConfigurationStore

import logging
logger = logging.getLogger(__name__)


class WikiConfigurationStoreTests(unittest.TestCase):
    wiki_configuration_store = None

    @classmethod
    def setUpClass(cls) -> None:
        cls.wiki_configuration_store = WikiConfigurationStore(
            engine=Database.setup_db("sqlite:///test_slackbot.db", "TEST"),
            logger=logger
        )

    def test_can_save_new_wiki_configuration_to_database_and_then_retrieve_it_from_database(self):
        configuration = {
            "team_id": "12345",
            "host_url": "https://example.com/",
            "project_id": 1234,
            "slug": "wiki/home",
            "access_token": "access_token"
        }
        self.wiki_configuration_store.save_wiki_configuration(configuration)
        stored_configuration = self.wiki_configuration_store.get_wiki_configuration(configuration["team_id"])
        self.assertEqual(configuration["team_id"], stored_configuration["team_id"])
        self.assertEqual(configuration["host_url"], stored_configuration["host_url"])
        self.assertEqual(configuration["project_id"], stored_configuration["project_id"])
        self.assertEqual(configuration["slug"], stored_configuration["slug"])
        self.assertEqual(configuration["access_token"], stored_configuration["access_token"])

    def test_save_wiki_configuration_when_configuration_already_exists_for_team(self):
        original_configuration = {
            "team_id": "12345",
            "host_url": "https://example.com/",
            "project_id": 1234,
            "slug": "wiki/home",
            "access_token": "access_token"
        }
        self.wiki_configuration_store.save_wiki_configuration(original_configuration)

        stored_configuration = self.wiki_configuration_store.get_wiki_configuration(original_configuration["team_id"])
        self.assertEqual(original_configuration["team_id"], stored_configuration["team_id"])
        self.assertEqual(original_configuration["host_url"], stored_configuration["host_url"])
        self.assertEqual(original_configuration["project_id"], stored_configuration["project_id"])
        self.assertEqual(original_configuration["slug"], stored_configuration["slug"])
        self.assertEqual(original_configuration["access_token"], stored_configuration["access_token"])

        new_configuration = {
            "team_id": "12345",
            "host_url": "https://example2.com/",
            "project_id": 123,
            "slug": "wiki/decisions",
            "access_token": "access_token"
        }
        self.wiki_configuration_store.save_wiki_configuration(new_configuration)

        stored_configuration = self.wiki_configuration_store.get_wiki_configuration(new_configuration["team_id"])
        self.assertEqual(new_configuration["team_id"], stored_configuration["team_id"])
        self.assertEqual(new_configuration["host_url"], stored_configuration["host_url"])
        self.assertEqual(new_configuration["project_id"], stored_configuration["project_id"])
        self.assertEqual(new_configuration["slug"], stored_configuration["slug"])
        self.assertEqual(new_configuration["access_token"], stored_configuration["access_token"])

    def test_get_wiki_configuration_with_configuration_that_does_not_exist(self):
        stored_configuration = self.wiki_configuration_store.get_wiki_configuration("does_not_exist")
        self.assertIsNone(stored_configuration)


if __name__ == '__main__':
    unittest.main()
