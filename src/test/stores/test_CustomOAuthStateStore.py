import unittest
from data import Database
from data.stores.CustomOAuthStateStore import CustomOAuthStateStore
import logging

logger = logging.getLogger(__name__)


class CustomOAuthStoreTests(unittest.TestCase):

    def test_issue_and_consume_where_state_has_not_expired(self):
        oauth_store = CustomOAuthStateStore(
            expiration_seconds=120,
            engine=Database.setup_db("sqlite:///test_slackbot.db", "TEST"),
            logger=logger
        )
        valid_state = oauth_store.issue()
        self.assertEqual(True, oauth_store.consume(valid_state))

    def test_issue_and_consume_where_state_has_expired(self):
        oauth_store = CustomOAuthStateStore(
            expiration_seconds=-10,
            engine=Database.setup_db("sqlite:///test_slackbot.db", "TEST"),
            logger=logger
        )
        expired_state = oauth_store.issue()
        self.assertEqual(False, oauth_store.consume(expired_state))


if __name__ == '__main__':
    unittest.main()
