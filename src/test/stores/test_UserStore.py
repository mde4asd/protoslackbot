import unittest
from data import Database
from data.stores.UserStore import UserStore
from sqlalchemy.orm import Session
from sqlalchemy.sql import text
from data.entities import Entities
import logging
import os

logger = logging.getLogger(__name__)


class UserStoreTests(unittest.TestCase):
    engine = None
    user_store = None

    @classmethod
    def setUpClass(cls) -> None:
        cls.engine = Database.setup_db("sqlite:///test_slackbot_stores.db", "TEST")
        cls.user_store = UserStore(
            engine=cls.engine,
            logger=logger
        )
        # insert some test users
        curr_dir = os.path.dirname(__file__)
        sql_script_path = os.path.join(curr_dir, "scripts/test_data_for_data_stores.sql")
        with Session(cls.engine) as session:
            session.begin()
            with open(sql_script_path) as file:
                statement = file.readline()
                while statement:
                    if statement.startswith("INSERT"):
                        query = text(statement)
                        session.execute(query)
                    statement = file.readline()
            session.commit()

    @classmethod
    def tearDownClass(cls) -> None:
        # remove test data
        with Session(cls.engine) as session:
            session.begin()
            for table in reversed(Entities.Base.metadata.sorted_tables):
                session.execute(table.delete())
            session.commit()

    def test_get_user_id_by_slack_id_returns_user_id_when_user_exists_with_slack_id(self):
        self.assertEqual(self.user_store.get_user_id_by_slack_id("1"), 1)

    def test_get_user_id_by_slack_id_returns_no_user_id_when_user_does_not_exist_with_slack_id(self):
        self.assertIsNone(self.user_store.get_user_id_by_slack_id("-1"))

    def test_get_user_id_by_metacontent_id_returns_user_id_when_user_exists_with_metacontent_id(self):
        self.assertEqual(self.user_store.get_user_id_by_metacontent_id("1"), 1)

    def test_get_user_id_by_metacontent_id_returns_no_user_id_when_user_does_not_exist_with_metacontent_id(self):
        self.assertIsNone(self.user_store.get_user_id_by_metacontent_id("-1"))

    def test_get_user_by_slack_id_and_name_returns_user_when_user_exists_with_slack_id_and_name(self):
        expected_user = {
            "slack_id": "2",
            "name": "Test User 2"
        }
        actual_user = self.user_store.get_user_by_slack_id_and_name(expected_user["slack_id"], expected_user["name"])
        self.assertEqual(actual_user["slack_id"], expected_user["slack_id"])
        self.assertEqual(actual_user["name"], expected_user["name"])

    def test_get_user_by_slack_id_and_name_returns_no_users_when_user_does_not_exist_with_slack_id_and_name(self):
        fake_user = {
            "slack_id": "-1",
            "name": "Test User -1"
        }
        actual_user = self.user_store.get_user_by_slack_id_and_name(fake_user["slack_id"], fake_user["name"])
        self.assertIsNone(actual_user)

    def test_get_user_by_id_returns_user_when_user_exists_wih_id(self):
        expected_user = {
            "slack_id": "3",
            "name": "Test User 3"
        }
        actual_user = self.user_store.get_user_by_id(3)
        self.assertEqual(actual_user["slack_id"], expected_user["slack_id"])
        self.assertEqual(actual_user["name"], expected_user["name"])

    def test_get_user_by_id_returns_no_user_when_user_does_not_exist_with_id(self):
        actual_user = self.user_store.get_user_by_id(-1)
        self.assertIsNone(actual_user)
