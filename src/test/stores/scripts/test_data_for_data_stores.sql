-- Design Decisions
INSERT INTO design_decisions (text) VALUES ("We will use MFA for login though it might take effort to implement.");
INSERT INTO design_decisions (text) VALUES ("In the login use case, I think we should use gallagher scan-cards for authentication.");
INSERT INTO design_decisions (text) VALUES ("In the add user use case, we need to consider speed and reliability so use of a relational DB would be better than using SQLite. Though it may take extra effort to convert");
INSERT INTO design_decisions (text) VALUES  ("In the context of sign-in to the system, we chose Single sign-on with Google and neglected email sign-in so that we could achieve confidentiality and integrity. The downside being that we depend on Google SSO API.");

-- Metacontent for Design Decisions
INSERT INTO metacontent (message_id, team_id, channel_id, decision_id) VALUES ("1", "1", "1", "1");
INSERT INTO metacontent (message_id, team_id, channel_id, decision_id) VALUES ("2", "2", "2", "2");
INSERT INTO metacontent (message_id, team_id, channel_id, decision_id) VALUES ("3", "3", "3", "3");
INSERT INTO metacontent (message_id, team_id, channel_id, decision_id) VALUES ("4", "4", "4", "4");

-- Users who Recorded Design Decisions
INSERT INTO users (slack_id, name) VALUES ("1", "Test User 1");
INSERT INTO users (slack_id, name) VALUES ("2", "Test User 2");
INSERT INTO users (slack_id, name) VALUES ("3", "Test User 3");
INSERT INTO users (slack_id, name) VALUES ("4", "Test User 4");

-- Metacontent - Users Association
INSERT INTO metacontent_user (metacontent_id, user_id) VALUES (1, 1);
INSERT INTO metacontent_user (metacontent_id, user_id) VALUES (2, 2);
INSERT INTO metacontent_user (metacontent_id, user_id) VALUES (3, 3);
INSERT INTO metacontent_user (metacontent_id, user_id) VALUES (4, 4);


-- Contexts
INSERT INTO context (context, decision_id) VALUES ("We will use MFA for login", 1);

-- Non-functional concerns
INSERT INTO non_functional_concerns (concern, decision_id) VALUES ("we need to consider speed and reliability", 3);

-- Options
INSERT INTO options (options, decision_id) VALUES ("I think we should use gallagher scan-cards for authentication.", 2);

-- Quality Attributes
INSERT INTO quality_attributes (name, decision_id) VALUES ("so that we could achieve confidentiality and integrity.", 4);

-- Consequences
INSERT INTO consequences (consequence, decision_id) VALUES ("though it might take effort to implement.", 1)
