import unittest
from data import Database
from sqlalchemy.orm import Session
from sqlalchemy.sql import text
from data.stores.DesignDecisionStore import DesignDecisionStore
from data.stores.UserStore import UserStore
from data.entities import Entities
import logging
import os

logger = logging.getLogger(__name__)


class DesignDecisionStoreTests(unittest.TestCase):
    def setUp(self):
        self.engine = Database.setup_db("sqlite:///test_slackbot_stores.db", "TEST")
        self.user_store = UserStore(
            engine=self.engine,
            logger=logger
        )
        self.design_decision_store = DesignDecisionStore(
            engine=self.engine,
            user_store=self.user_store,
            logger=logger
        )
        # insert some test data
        curr_dir = os.path.dirname(__file__)
        sql_script_path = os.path.join(curr_dir, "scripts/test_data_for_data_stores.sql")
        with Session(self.engine) as session:
            session.begin()
            with open(sql_script_path) as file:
                statement = file.readline()
                while statement:
                    if statement.startswith("INSERT"):
                        query = text(statement)
                        session.execute(query)
                    statement = file.readline()
            session.commit()

    def tearDown(self):
        # remove test data
        with Session(self.engine) as session:
            session.begin()
            for table in reversed(Entities.Base.metadata.sorted_tables):
                session.execute(table.delete())
            session.commit()

    def test_create_design_decision_with_valid_decision_metacontent_and_new_user_will_commit(self):
        """
        Test that creating a new design decision with a new user will be result in a successful commit to the
        database.
        :return: None, as a test
        """
        valid_data = {
            "text": "Design decision text one.",
            "ts": "1",
            "team": "1",
            "channel": "1"
        }
        valid_user = {
            "slack_id": "TESTUSER1",
            "name": "Test User the First"
        }
        # Create new design decision
        self.design_decision_store.create_design_decision(valid_data, valid_user)
        # A DesignDecision with text, valid_data["text"], should now exist
        stored_design_decision = self.design_decision_store.get_design_decision_by_text(valid_data["text"])
        self.assertEqual(valid_data["text"], stored_design_decision["text"])
        # A MetaContent with message_id, valid_data["ts"], team_id, valid_data["team"], and channel_id,
        # valid_data["channel"], should now exist
        stored_metacontent = self.design_decision_store.get_metacontent_by_message_id_team_id_and_channel_id(
            valid_data["ts"], valid_data["team"], valid_data["channel"]
        )
        self.assertEqual(valid_data["ts"], stored_metacontent["message_id"])
        self.assertEqual(valid_data["team"], stored_metacontent["team_id"])
        self.assertEqual(valid_data["channel"], stored_metacontent["channel_id"])
        # A User with slack_id, valid_user["slack_id"], and name, valid_user["name"], should now exist
        stored_user = self.user_store.get_user_by_slack_id_and_name(
            valid_user["slack_id"], valid_user["name"]
        )
        self.assertEqual(valid_user["slack_id"], stored_user["slack_id"])
        self.assertEqual(valid_user["name"], stored_user["name"])

    def test_create_design_decision_with_valid_decision_metacontent_and_existing_user_will_commit(self):
        """
        Test that creating a new design decision with an existing user will be result in a successful commit to the
        database.
        :return: None, as a test
        """
        valid_data_1 = {
            "text": "Design decision text two.",
            "ts": "2",
            "team": "2",
            "channel": "2"
        }
        valid_data_2 = {
            "text": "Design decision text three.",
            "ts": "3",
            "team": "3",
            "channel": "3"
        }
        valid_user = {
            "slack_id": "TESTUSER2",
            "name": "Test User the Second"
        }
        # New user creates new design decision
        self.design_decision_store.create_design_decision(valid_data_1, valid_user)
        # Existing user creates new design decision
        self.design_decision_store.create_design_decision(valid_data_2, valid_user)
        # A DesignDecision with text, valid_data_2["text"], should exist
        stored_design_decision = self.design_decision_store.get_design_decision_by_text(valid_data_2["text"])
        self.assertEqual(valid_data_2["text"], stored_design_decision["text"])
        # A MetaContent with message_id, valid_data_2["ts"], team_id, valid_data_2["team"], and channel_id,
        # valid_data_2["channel"], should exist
        stored_metacontent = self.design_decision_store.get_metacontent_by_message_id_team_id_and_channel_id(
            valid_data_2["ts"], valid_data_2["team"], valid_data_2["channel"]
        )
        self.assertEqual(valid_data_2["ts"], stored_metacontent["message_id"])
        self.assertEqual(valid_data_2["team"], stored_metacontent["team_id"])
        self.assertEqual(valid_data_2["channel"], stored_metacontent["channel_id"])
        # A User with slack_id, valid_user["slack_id"], and name, valid_user["name"], should exist
        stored_user = self.user_store.get_user_by_slack_id_and_name(
            valid_user["slack_id"], valid_user["name"]
        )
        self.assertEqual(valid_user["slack_id"], stored_user["slack_id"])
        self.assertEqual(valid_user["name"], stored_user["name"])

    def test_create_design_decision_with_invalid_decision_and_valid_metacontent_and_user_will_rollback(self):
        """
        Test that creating a new design decision with invalid design decision data will result in the design decision
        not being committed to the database and an exception raised instead
        :return: None, as a test
        """
        invalid_data = {
            # missing "text" field - invalid data
            "ts": "4",
            "team": "4",
            "channel": "4"
        }
        valid_user = {
            "slack_id": "TESTUSER4",
            "name": "Test User the Third"
        }
        self.assertRaises(KeyError, self.design_decision_store.create_design_decision, invalid_data, valid_user)

    def test_create_design_decision_with_valid_decision_and_user_and_invalid_metacontent_will_rollback(self):
        """
        Test that creating a new design decision with invalid metacontent data will result in the design decision
        not being committed to the database and an exception raised instead
        :return: None, as a test
        """
        invalid_data = {
            "text": "Design decision text five",
            # missing "ts" field - invalid data
            "team": "5",
            "channel": "5"
        }
        valid_user = {
            "slack_id": "TESTUSER4",
            "name": "Test User the Fourth"
        }
        self.assertRaises(KeyError, self.design_decision_store.create_design_decision, invalid_data, valid_user)

    def test_create_design_decision_with_valid_decision_and_metacontent_and_invalid_user_will_rollback(self):
        """
        Test that creating a new design decision with invalid user data will result in the design decision
        not being committed to the database and an exception raised instead
        :return: None, as a test
        """
        valid_data = {
            "text": "Design decision text six",
            "ts": "6",
            "team": "6",
            "channel": "6"
        }
        invalid_user = {
            # missing "slack_id" field - invalid data
            "name": "Test User the Fifth"
        }
        self.assertRaises(KeyError, self.design_decision_store.create_design_decision, valid_data, invalid_user)

    def test_can_create_and_search_for_design_decision(self):
        """
        The test firsts creates a design decision with valid parameters. The test then searches for the stored
        design decision. If there is a result then the design decision has successfully been saved.
        :return: None, as test
        """
        text = "Design decision text seven"
        team_id = "7"
        data = {
            "text": text,
            "ts": "7",
            "team": team_id,
            "channel": "7"
        }
        user = {
            "slack_id": "TESTUSER6",
            "name": "Test User the Sixth"
        }
        self.design_decision_store.create_design_decision(data, user)
        results = self.design_decision_store.search_design_decisions(team_id, text)
        self.assertEqual(results[0][0]["text"], text)

    def test_delete_design_decision_component_by_content_when_matching_context_exists_should_delete_context_and_return_label(
            self):
        context_content = "We will use MFA for login"
        message_id = "1"
        decision_id, label = self.design_decision_store.delete_design_decision_component_by_content(
            context_content, message_id
        )
        self.assertEqual(decision_id, int(message_id))
        self.assertEqual(label, "USE CASE: DELETED")

    def test_delete_design_decision_component_by_content_when_matching_non_functional_concern_exists_should_delete_non_functional_concern_and_return_label(
            self):
        non_functional_concern_content = "we need to consider speed and reliability"
        message_id = "3"
        decision_id, label = self.design_decision_store.delete_design_decision_component_by_content(
            non_functional_concern_content, message_id
        )
        self.assertEqual(decision_id, int(message_id))
        self.assertEqual(label, "NON FUNCTIONAL CONCERN: DELETED")

    def test_delete_design_decision_component_by_content_when_matching_options_exists_should_delete_options_and_return_label(
            self):
        options_content = "I think we should use gallagher scan-cards for authentication."
        message_id = "2"
        decision_id, label = self.design_decision_store.delete_design_decision_component_by_content(
            options_content, message_id
        )
        self.assertEqual(decision_id, int(message_id))
        self.assertEqual(label, "OPTION: DELETED")

    def test_delete_design_decision_component_by_content_when_matching_quality_attribute_exists_should_delete_quality_attribute_and_return_label(
            self):
        quality_attribute_content = "so that we could achieve confidentiality and integrity."
        message_id = "4"
        decision_id, label = self.design_decision_store.delete_design_decision_component_by_content(
            quality_attribute_content, message_id
        )
        self.assertEqual(decision_id, int(message_id))
        self.assertEqual(label, "QUALITY ATTRIBUTES: DELETED")

    def test_delete_design_decision_component_by_content_when_matching_consequence_exists_should_delete_consequence_and_return_label(
            self):
        consequence_content = "though it might take effort to implement."
        message_id = "1"
        decision_id, label = self.design_decision_store.delete_design_decision_component_by_content(
            consequence_content, message_id
        )
        self.assertEqual(decision_id, int(message_id))
        self.assertEqual(label, "CONSEQUENCES: DELETED")

    def test_delete_design_decision_component_by_content_when_no_matching_component_exists_should_return_no_label(self):
        content = "This does not exist"
        message_id = "1"
        decision_id, label = self.design_decision_store.delete_design_decision_component_by_content(
            content, message_id
        )
        self.assertEqual(decision_id, int(message_id))
        self.assertIsNone(label)

    def test_amend_design_decision_when_original_recorder_updates_design_decision(self):
        decision_id = 1
        message_id = "1"
        design_decision = "An updated design decision."
        user = {
            "slack_id": "1",
            "name": "Test User 1"
        }
        # original design decision
        original_decision = self.design_decision_store.get_design_decision_by_id(decision_id)
        self.assertNotEqual(original_decision["text"], design_decision)

        # amending design decision
        self.design_decision_store.amend_design_decision(decision_id, design_decision, user, message_id)

        # design decision should be updated
        amended_decision = self.design_decision_store.get_design_decision_by_id(decision_id)
        self.assertEqual(amended_decision["text"], design_decision)

    def test_amend_design_decision_when_new_user_updates_design_decision(self):
        decision_id = 1
        message_id = "1"
        design_decision = "An updated design decision."
        updated_by_user_data = {
            "slack_id": "2",
            "name": "Test User 2"
        }
        # original design decision with original recorder
        original_decision = self.design_decision_store.get_design_decision_by_id(decision_id)
        metacontent_id = self.design_decision_store.get_metacontent_id_by_decision_id(decision_id)
        original_recorder_id = self.user_store.get_user_id_by_metacontent_id(metacontent_id)
        original_recoder = self.user_store.get_user_by_id(original_recorder_id)
        self.assertNotEqual(original_decision["text"], design_decision)
        self.assertNotEqual(original_recoder["slack_id"], updated_by_user_data["slack_id"])
        self.assertNotEqual(original_recoder["name"], updated_by_user_data["name"])

        # amending design decision
        self.design_decision_store.amend_design_decision(decision_id, design_decision, updated_by_user_data, message_id)

        # design decision should be updated with user who updated
        amended_decision = self.design_decision_store.get_design_decision_by_id(decision_id)
        updated_by_user_id = self.user_store.get_user_id_by_metacontent_id(metacontent_id)
        updated_by_user = self.user_store.get_user_by_id(updated_by_user_id)
        self.assertEqual(amended_decision["text"], design_decision)
        self.assertEqual(updated_by_user["slack_id"], updated_by_user_data["slack_id"])
        self.assertEqual(updated_by_user["name"], updated_by_user_data["name"])


if __name__ == '__main__':
    unittest.main()
