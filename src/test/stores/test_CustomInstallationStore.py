import unittest
from data import Database
from data.stores.CustomInstallationStore import CustomInstallationStore
import logging
from slack_sdk.oauth.installation_store.models.bot import Bot
import time
from datetime import datetime

logger = logging.getLogger(__name__)


class CustomInstallationStoreTests(unittest.TestCase):
    installation_store = None

    @classmethod
    def setUpClass(cls) -> None:
        cls.installation_store = CustomInstallationStore(
            client_id="client_id",
            engine=Database.setup_db("sqlite:///test_slackbot.db", "TEST"),
            logger=logger
        )

    def test_save_bot_and_find_bot_methods_with_valid_bot(self):
        enterprise_id = "enterprise_id"
        team_id = "team_id"
        is_enterprise_install = False
        new_bot = Bot(
            app_id="app_id",
            enterprise_id=enterprise_id,
            enterprise_name="enterprise_name",
            team_id=team_id,
            team_name="team_name",
            bot_token="bot_token",
            bot_id="bot_id",
            bot_user_id="bot_user_id",
            bot_scopes="bot_scopes",
            bot_refresh_token="bot_refresh_token",
            bot_token_expires_at=datetime.utcfromtimestamp(time.time()),
            is_enterprise_install=is_enterprise_install,
            installed_at=datetime.utcfromtimestamp(time.time()),
        )
        self.installation_store.save_bot(new_bot)
        found_bot = self.installation_store.find_bot(enterprise_id=enterprise_id, team_id=team_id,
                                                     is_enterprise_install=is_enterprise_install)
        self.assertEqual(new_bot.app_id, found_bot.app_id)
        self.assertEqual(new_bot.enterprise_id, found_bot.enterprise_id)
        self.assertEqual(new_bot.enterprise_name, found_bot.enterprise_name)
        self.assertEqual(new_bot.team_name, found_bot.team_name)
        self.assertEqual(new_bot.team_id, found_bot.team_id)
        self.assertEqual(new_bot.bot_token, found_bot.bot_token)
        self.assertEqual(new_bot.bot_user_id, found_bot.bot_user_id)
        self.assertEqual(new_bot.bot_user_id, found_bot.bot_user_id)
        self.assertEqual(new_bot.bot_scopes, found_bot.bot_scopes)
        self.assertEqual(new_bot.bot_refresh_token, found_bot.bot_refresh_token)
        self.assertEqual(new_bot.is_enterprise_install, found_bot.is_enterprise_install)

    def test_find_bot_with_bot_that_does_not_exist(self):
        found_bot = self.installation_store.find_bot(enterprise_id="does_not_exist", team_id="does_not_exist",
                                                     is_enterprise_install=False)
        self.assertIsNone(found_bot)


if __name__ == '__main__':
    unittest.main()
