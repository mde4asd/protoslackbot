import unittest
from outputters import MarkdownParser


class Page:
    content: str

    def __init__(self, content):
        self.content = content

    def __repr__(self):
        print(self.content)


class MarkdownParserTests(unittest.TestCase):
    def test_get_component_coloured_text_when_label_is_context_component_returns_blue_text(self):
        sentence = "In the add user use case"
        use_case_text = MarkdownParser.get_component_coloured_text(sentence, "USE CASE")
        component_text = MarkdownParser.get_component_coloured_text(sentence, "COMPONENT")
        expected_text = MarkdownParser.COLOUR_TEXT_STRING % (MarkdownParser.SLACK_BLUE, sentence)
        self.assertEqual(use_case_text, expected_text)
        self.assertEqual(component_text, expected_text)

    def test_get_component_coloured_text_when_label_is_non_functional_concern_component_returns_yellow_text(self):
        sentence = ", we need to consider speed and reliability"
        concern_text = MarkdownParser.get_component_coloured_text(sentence, "NON FUNCTIONAL CONCERN")
        expected_text = MarkdownParser.COLOUR_TEXT_STRING % (MarkdownParser.SLACK_YELLOW, sentence)
        self.assertEqual(concern_text, expected_text)

    def test_get_component_coloured_text_when_label_is_options_component_returns_red_text(self):
        sentence = "so use of a relational DB would be better than using SQLite."
        option_text = MarkdownParser.get_component_coloured_text(sentence, "OPTION")
        other_options_text = MarkdownParser.get_component_coloured_text(sentence, "OTHER OPTIONS")
        expected_text = MarkdownParser.COLOUR_TEXT_STRING % (MarkdownParser.SLACK_RED, sentence)
        self.assertEqual(option_text, expected_text)
        self.assertEqual(other_options_text, expected_text)

    def test_get_component_coloured_text_when_label_is_quality_attribute_component_returns_green_text(self):
        sentence = ", we need to consider speed and reliability"
        quality_attribute_text = MarkdownParser.get_component_coloured_text(sentence, "QUALITY ATTRIBUTES")
        expected_text = MarkdownParser.COLOUR_TEXT_STRING % (MarkdownParser.SLACK_GREEN, sentence)
        self.assertEqual(quality_attribute_text, expected_text)

    def test_get_component_coloured_text_when_label_is_consequences_component_returns_purple_text(self):
        sentence = "Though it may take extra effort to convert @Bot"
        consequence_text = MarkdownParser.get_component_coloured_text(sentence, "CONSEQUENCES")
        expected_text = MarkdownParser.COLOUR_TEXT_STRING % (MarkdownParser.SLACK_PURPLE, sentence)
        self.assertEqual(consequence_text, expected_text)

    def test_get_component_coloured_text_when_label_is_invalid_component_raises_error(self):
        sentence = "This is a random sentence."
        with self.assertRaises(ValueError):
            MarkdownParser.get_component_coloured_text(sentence, "INVALID TYPE")


if __name__ == '__main__':
    unittest.main()
