import unittest
from unittest.mock import Mock


class AppTests(unittest.TestCase):
    def test_save_to_json(self):
        sentence = "This is a unit test"
        label = "USE_CASE"
        thread_ts = "24242424"
        app = Mock()
        app.write_to_file(sentence, thread_ts, label=label, remove=False)
        self.assertEqual(True, True)  # add assertion here


if __name__ == '__main__':
    unittest.main()
