import unittest
from data.entities.DesignDecisionParser import parse_design_decision_component
from data.entities import Entities


class DataParserTests(unittest.TestCase):
    def test_parse_design_decision_component_returns_parsed_context_when_label_is_use_case_or_component(self):
        sentence = "In the add user use case"
        decision_id = 1
        parsed_use_case = parse_design_decision_component(sentence, "USE CASE", decision_id)
        parsed_component = parse_design_decision_component(sentence, "COMPONENT", decision_id)
        self.assertIsInstance(parsed_use_case, Entities.Context)
        self.assertIsInstance(parsed_component, Entities.Context)

    def test_parse_design_decision_component_returns_parsed_non_functional_concern(self):
        sentence = ", we need to consider speed and reliability"
        decision_id = 1
        parsed_concern = parse_design_decision_component(sentence, "NON FUNCTIONAL CONCERN", decision_id)
        self.assertIsInstance(parsed_concern, Entities.NonFunctionalConcern)

    def test_parse_design_decision_component_returns_parsed_options(self):
        sentence = "so use of a relational DB would be better than using SQLite."
        decision_id = 1
        parsed_option = parse_design_decision_component(sentence, "OPTION", decision_id)
        parsed_other_options = parse_design_decision_component(sentence, "OTHER OPTIONS", decision_id)
        self.assertIsInstance(parsed_option, Entities.Options)
        self.assertIsInstance(parsed_other_options, Entities.Options)

    def test_parse_design_decision_component_returns_parsed_quality_attribute(self):
        sentence = ", we need to consider speed and reliability"
        decision_id = 1
        parsed_quality_attribute = parse_design_decision_component(sentence, "QUALITY ATTRIBUTES", decision_id)
        self.assertIsInstance(parsed_quality_attribute, Entities.QualityAttribute)

    def test_parse_design_decision_component_returns_parsed_consequences(self):
        sentence = "Though it may take extra effort to convert @Bot"
        decision_id = 1
        parsed_consequence = parse_design_decision_component(sentence, "CONSEQUENCES", decision_id)
        self.assertIsInstance(parsed_consequence, Entities.Consequence)

    def test_parse_design_decision_component_raises_error_when_label_does_not_match_valid_component_type(self):
        sentence = "This is a random sentence."
        decision_id = 1
        with self.assertRaises(ValueError):
            parse_design_decision_component(sentence, "INVALID TYPE", decision_id)


if __name__ == '__main__':
    unittest.main()
