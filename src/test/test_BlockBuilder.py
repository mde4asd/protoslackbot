import unittest
from util import BlockBuilder
import datetime


class BlockBuilderTests(unittest.TestCase):

    def test_build_search_result_block_when_the_number_of_results_is_greater_then_zero(self):
        text = 'In the add user use case, we need to consider speed and reliability so use of a relational DB would ' \
               'be better than using SQLite. Though it may take extra effort to convert'
        date = datetime.datetime(2022, 7, 10, 22, 43, 25)
        name = "Test User"
        decision_id = 1
        results = [(decision_id, text, 1, date, name)]
        query_string = "A query string"
        total_rows = 1

        expected_blocks = [
            {
                "type": "section",
                "text": {
                    "type": "mrkdwn",
                    "text": f":mag: The results of the search: _{query_string}_"
                }
            },
            {
                "type": "divider"
            },
            {
                "type": "section",
                "text": {
                    "type": "mrkdwn",
                    "text": "*Date* | *Recorded By* | *Decision Id* | *Design Decision*"
                }
            },
            {
                "type": "divider"
            },
            {
                "type": "section",
                "text": {
                    "type": "plain_text",
                    "text": "| {} | {} | {} |\n{}".format(date.strftime("%d/%m/%Y"), name, decision_id, text)
                }
            },
            {
                "type": "divider"
            }
        ]

        blocks = BlockBuilder.build_search_result_block(results, query_string, total_rows)
        self.assertEqual(expected_blocks, blocks)

    def test_build_search_result_block_when_the_number_of_results_is_zero(self):
        results = []
        query_string = "A query string"
        total_rows = 0
        expected_blocks = [
            {
                "type": "section",
                "text": {
                    "type": "mrkdwn",
                    "text": f":mag: The results of the search: _{query_string}_"
                }
            },
            {
                "type": "divider"
            },
            {
                "type": "section",
                "text": {
                    "type": "plain_text",
                    "text": "No design decisions matching the search criteria were found."
                }
            }
        ]
        blocks = BlockBuilder.build_search_result_block(results, query_string, total_rows)
        self.assertEqual(expected_blocks, blocks)


if __name__ == '__main__':
    unittest.main()
