The Slackbot uses a machine learning model to split and classify complex sentences representing design decisions 
“roughly” following the WH(Y) template.

<div align="center">
  <img src="%s" alt="WH(Y) model"/>
  <figcaption>Fig. 1 - The WH(Y) model</figcaption>
</div>

<br>

In the recorded design decisions table, the different components follow the colour scheme: 
$`\textcolor{#00394A}{\textsf{context}}`$,
$`\textcolor{#CC8E00}{\textsf{non-functional concern}}`$,
$`\textcolor{#88002A}{\textsf{options}}`$,
$`\textcolor{#007745}{\textsf{quality attribute}}`$, and
$`\textcolor{#580063}{\textsf{consequences}}`$.

*Disclaimer: This page is generated by the Slackbot. If changes are made to design decisions here, they will not be 
reflected in Slack. Additionally, making changes to this page may affect the generation by the Slackbot. It is 
recommended that NO changes are made to this page.* 

| Date | Recorded By | Decision Id | Design Decision | 
|------|-------------|-------------|-----------------|