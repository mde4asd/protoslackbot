import logging
from logging import Logger
from outputters.DesignDecisionOutputter import DesignDecisionOutputter
import gitlab
from gitlab import Gitlab, GitlabError, GitlabAuthenticationError, GitlabGetError
from data.entities.Entities import WikiConfiguration
import os
from outputters import MarkdownParser

static_logger = logging.getLogger(__name__)


class GitLabOutputter(DesignDecisionOutputter):

    wiki_configuration: WikiConfiguration
    gitlab_connection: Gitlab

    def __init__(
            self,
            wiki_configuration: WikiConfiguration,
            logger: Logger = logging.getLogger(__name__)
    ):
        self.wiki_configuration = wiki_configuration
        self._logger = logger

    @property
    def logger(self) -> Logger:
        if self._logger is None:
            self._logger = logging.getLogger(__name__)
        return self._logger

    def connect_to_gitlab(self):
        self.gitlab_connection = gitlab.Gitlab(
            url=self.wiki_configuration["host_url"],
            private_token=self.wiki_configuration["access_token"]
        )

    def get_project(self):
        return self.gitlab_connection.projects.get(self.wiki_configuration["project_id"])

    def setup_wiki_page(self):
        """
        This methods checks to see whether the wiki page the design decisions are outputted to can be retrieved.
        If the page can not be retrieved then the reason why is returned.
        :return: page, if can be retrieved, None and error message otherwise.
        """
        try:
            self.connect_to_gitlab()
            project = self.get_project()
            page = self.get_wiki_page(project)
            return page, f"No error {page}"
        except ConnectionError:
            return None, "Host URL is invalid"
        except GitlabAuthenticationError:
            return None, "Access Token is invalid"
        except GitlabGetError:
            return None, "Project ID is invalid"
        except Exception as e:
            self._logger.error(e)
            return None, "An unknown error occurred"

    def get_wiki_page(self, project):
        try:
            return project.wikis.get(self.wiki_configuration["slug"])
        except GitlabError as e:
            self._logger.error(e)
            self._logger.info("Creating new wiki page...")
            return self.create_wiki_page(project)

    def create_wiki_page(self, project):
        """
        This method creates a new wiki page for the recorded design decisions.
        1). Uploads an image of the WH(Y) model that will be included on the wiki page.
        2). Creates the content for the wiki page from a template.
        3). Uploads the wiki page to GitLab
        :param project: a GitLab project instance
        :return page, a newly created wiki page that will contain the recorded design decisions
        """
        uploaded_image = project.upload("why.png", filepath=os.path.join(os.path.dirname(__file__), "why.png"))
        template = open(os.path.join(os.path.dirname(__file__), "WIKI_TEMPLATE.md"), "r")
        content = template.read() % uploaded_image["url"]
        template.close()
        page = project.wikis.create({
            'title': self.wiki_configuration["slug"],
            'content': content
        })
        return page

    def output_decision(self, decision_id, recorder, design_decision):
        page, error = self.setup_wiki_page()
        if page:
            update_wiki_page_with_decision(page, decision_id, recorder, design_decision)
        else:
            self._logger.error(error)

    def output_component(self, decision_id, sentence, label):
        page, error = self.setup_wiki_page()
        if page:
            update_wiki_page_with_component(page, decision_id, sentence, label)
        else:
            self._logger.error(error)

    def get_wiki_url(self):
        return self.get_project().attributes["web_url"] + "/-/wikis/" + self.wiki_configuration["slug"]


def create_gitlab_outputter(wiki_configuration, logger):
    try:
        return GitLabOutputter(wiki_configuration=wiki_configuration, logger=logger)
    except Exception as e:
        static_logger.error(f"Wiki configuration is not valid ({e}).")
        return None


def update_wiki_page_with_decision(page, decision_id, recorder, design_decision):
    page.content = MarkdownParser.parse_design_decision(page, decision_id, recorder, design_decision)
    page.save()


def update_wiki_page_with_component(page, decision_id, sentence, label):
    page.content = MarkdownParser.parse_decision_component(page, decision_id, sentence, label)
    page.save()
