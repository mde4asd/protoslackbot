from logging import Logger


class DesignDecisionOutputter:
    @property
    def logger(self) -> Logger:
        raise NotImplementedError()

    def output_decision(self, *args, **kwargs) -> None:
        raise NotImplementedError()

    def output_component(self, *args, **kwargs) -> None:
        raise NotImplementedError()
