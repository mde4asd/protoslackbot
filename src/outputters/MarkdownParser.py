from datetime import date
from util import SlackEventParser
from model.Candidate import Candidate
import logging

logger = logging.getLogger(__name__)

SLACK_BLUE = "#00394A"
SLACK_YELLOW = "#CC8E00"
SLACK_RED = "#88002A"
SLACK_GREEN = "#007745"
SLACK_PURPLE = "#580063"
COLOUR_TEXT_STRING = "$`\\textcolor{%s}{\\textsf{%s}}`$"
FIRST_DECISION_ID_INDEX = 13
NEXT_DECISION_ID_INDEX = 5
NEXT_DESIGN_DECISION_INDEX = 1
NEXT_RECORDER_INDEX = 1
NEXT_DATE_INDEX = 2


def parse_design_decision(page, decision_id, recorder, decision):
    current_content = page.content
    content_items = current_content.split("|")
    decision_id_index = FIRST_DECISION_ID_INDEX
    today = date.today().strftime("%d/%m/%Y")
    updated_decision = False
    for i in range(decision_id_index, len(content_items)):
        if looped_through_all_decisions(decision_id_index, content_items):
            break
        if design_decision_found(content_items, decision_id_index, decision_id):
            current_design_decision_index = decision_id_index + NEXT_DESIGN_DECISION_INDEX
            content_items[current_design_decision_index] = SlackEventParser.sanitise_user_input(decision)
            content_items[decision_id_index - NEXT_RECORDER_INDEX] = recorder
            content_items[decision_id_index - NEXT_DATE_INDEX] = today
            updated_decision = True
        decision_id_index += NEXT_DECISION_ID_INDEX
    new_content = "|".join(content_items)
    if not updated_decision:
        new_content = current_content + f"\n| {today} | {recorder} | {decision_id} | {SlackEventParser.sanitise_user_input(decision)} |"
    return new_content


def parse_decision_component(page, decision_id, sentence, label):
    """
    This method takes the current content of a wiki page and changes the colour of the text corresponding to
    a design decision component
    :param page: the current wiki page and its contents
    :param decision_id: the id of the design decision that has the component
    :param sentence: the sentence making up the design decision component
    :param label: the classification of the component e.g. USE CASE, OPTION etc.
    :return: updated content for the wiki page.
    """
    try:
        current_content = page.content
        content_items = current_content.split("|")
        decision_id_index = FIRST_DECISION_ID_INDEX
        for i in range(decision_id_index, len(content_items)):
            if looped_through_all_decisions(decision_id_index, content_items):
                break
            if design_decision_found(content_items, decision_id_index, decision_id):
                current_design_decision_index = decision_id_index + NEXT_DESIGN_DECISION_INDEX
                if component_classification_changed(label):
                    content_items[current_design_decision_index] = content_items[current_design_decision_index].replace(
                        get_component_coloured_text(sentence, label), sentence
                    )
                    break
                if component_not_classified(content_items, decision_id_index, sentence):
                    content_items[current_design_decision_index] = content_items[current_design_decision_index].replace(
                        sentence, get_component_coloured_text(sentence, label)
                    )
                    break
            decision_id_index += NEXT_DECISION_ID_INDEX
        new_content = "|".join(content_items)
        return new_content
    except Exception as e:
        logger.error(e)


def looped_through_all_decisions(decision_id_index, content_items):
    return decision_id_index >= len(content_items)


def design_decision_found(content_items, decision_id_index, decision_id):
    return str(content_items[decision_id_index].strip()) == str(decision_id)


def component_not_classified(content_items, decision_id_index, sentence):
    return content_items[decision_id_index + NEXT_DESIGN_DECISION_INDEX].find("{" + sentence.strip() + "}") == -1


def component_classification_changed(label):
    label_components = label.split(":")
    if len(label_components) == 2:
        return label_components[1].strip() == "DELETED"
    return None


def get_component_coloured_text(sentence, label):
    """
    This method generates the GitLab flavoured markdown for changing the colour of text
    based on the type of design decision component
    :param sentence: the sentence representing a design decision component
    :param label: the type of component
    :return: the GitLab flavoured markdown for changing the colour of text.
    """
    try:
        type_component = "_".join(label.split(":")[0].upper().split(" "))
        match Candidate[type_component]:
            case Candidate.USE_CASE | Candidate.COMPONENT:
                return COLOUR_TEXT_STRING % (SLACK_BLUE, sentence)
            case Candidate.NON_FUNCTIONAL_CONCERN:
                return COLOUR_TEXT_STRING % (SLACK_YELLOW, sentence)
            case Candidate.OPTION | Candidate.OTHER_OPTIONS:
                return COLOUR_TEXT_STRING % (SLACK_RED, sentence)
            case Candidate.QUALITY_ATTRIBUTES:
                return COLOUR_TEXT_STRING % (SLACK_GREEN, sentence)
            case Candidate.CONSEQUENCES:
                return COLOUR_TEXT_STRING % (SLACK_PURPLE, sentence)
            case _:
                raise ValueError("Not a valid design decision component type.")
    except KeyError as e:
        logger.error(e)
        raise ValueError("Not a valid design decision component type.") from None
