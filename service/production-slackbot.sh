#!/usr/bin/bash

source "env.txt"

# Run the staging slackbot application
docker run -p ${LOCAL_PORT}:${SLACK_PORT} -e ENVIRONMENT=${ENVIRONMENT} \
    -e SLACK_CLIENT_ID=${SLACK_CLIENT_ID} -e SLACK_SIGNING_SECRET=${SLACK_SIGNING_SECRET} -e SLACK_CLIENT_SECRET=${SLACK_CLIENT_SECRET} \
    -e DB_USERNAME=${DB_USERNAME} -e DB_PASSWORD=${DB_PASSWORD} -e HOST_URL=${HOST_URL} \
    --network=host registry.gitlab.com/mde4asd/protoslackbot:latest
