#syntax=docker/dockerfile:1

# BASE STAGE
FROM ubuntu:jammy AS base

# Install OS-level dependencies
RUN apt-get update && apt-get install -y python3 python3-pip python3.10-venv libmariadb3 libmariadb-dev \
    && apt-get clean && rm -rf /var/lib/apt/lists/*

# BUILDER STAGE
FROM base AS builder

# Create virtual environment
RUN python3 -m venv /opt/venv
ENV PATH=/opt/venv/bin:$PATH

# Install Python dependencies
WORKDIR /app
COPY build/requirements.txt requirements.txt
RUN python3 -m pip install --upgrade pip
RUN pip3 install --no-cache-dir torch
RUN pip3 install -r requirements.txt
RUN python3 -m spacy download en_core_web_sm

# RUNNER STAGE
FROM base

# Copy virtual environment
COPY --from=builder /opt/venv /opt/venv
ENV PATH=/opt/venv/bin:$PATH

# Copy source code into working director inside docker image
WORKDIR /app
COPY /src .

# Set up envrionment variables
ENV SLACK_SIGNING_SECRET=${SLACK_SIGNING_SECRET}
ENV SLACK_CLIENT_SECRET=${SLACK_CLIENT_SECRET}
ENV LOCAL_PORT=${LOCAL_PORT}
ENV SLACK_PORT=${SLACK_PORT}
ENV ENVIRONMENT=${ENVIRONMENT}
ENV DB_USERNAME=${DB_USERNAME}
ENV DB_PASSWORD=${DB_PASSWORD}
ENV SLACK_CLIENT_ID=${SLACK_CLIENT_ID}
ENV HOST_URL=${HOST_URL}

EXPOSE 3000

CMD ["python", "app.py"]