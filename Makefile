# Build docker image
# User the .env file if exists (which it should)
ifneq (,$(wildcard ./.env))
    include .env
    export
    ENV_FILE_PARAM = --env-file .env
endif

docker-dev:
	docker build -t protoslackbot:v0.1.0 -f Dockerfile .

docker-prod:
	docker build -t protoslackbot:v0.1.0 -f Dockerfile .

# run the docker image (after build from docker-prod) uses .env variables
run:
	docker run -d -p $(LOCAL_PORT):$(SLACK_PORT) \
	-e SLACK_SIGNING_SECRET=$(SLACK_SIGNING_SECRET) -e SLACK_CLIENT_SECRET=$(SLACK_CLIENT_SECRET) \
	-e ENVIRONMENT=$(ENVIRONMENT) -e DB_USERNAME=$(DB_USERNAME) -e DB_PASSWORD=$(DB_PASSWORD) \
	-e SLACK_CLIENT_ID=$(SLACK_CLIENT_ID) -e HOST_URL=$(HOST_URL) --network=host protoslackbot:v0.1.0