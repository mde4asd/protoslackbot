# Protoslackbot

***

Protoslackbot is a Slackbot that captures design decisions as they are discussed in Slack. 
This readme outlines two methods for how the Slackbot can be deployed locally. 
In each case, a .env file must exist locally. 
The file env.txt provides a template of the .env file and its secrets. 
For more details on what each environment variable is, check out the GitLab wiki here [CI/CD Pipeline](https://gitlab.com/mde4asd/protoslackbot/-/wikis/Getting%20Started/CI%20CD).

## Deploying locally:

***

1. `cd NGROK` ** you will need an authenticated account (this changed early 2021)
2. Start ngrok `./ngrok http 3000`
3. Ensure dependencies are installed to python-venv
   * `python3 -m pip install --upgrade pip` 
   * `pip3 install -r build/requirements.txt`
   * `python3 -m spacy download en_core_web_sm`
4. There must be an .env file in the working directory. A txt file is given as a template   
5. Copy https:// address from ngrok to [Slack Event Subscriptions](https://api.slack.com/apps/A026WE440RH/event-subscriptions?) `https://....ngrok.io/slack/events` and save
6. Due to checkbox interactivity there is a requirement to need to add it to [Slack Interactivity & Shortcuts](https://api.slack.com/apps/A026WE440RH/interactive-messages?) for checkboxes
7. Run `app.py`
8. (optional) set up the Slackbot to output recorded design decisions to a [Gitlab wiki](https://gitlab.com/mde4asd/protoslackbot/-/wikis/Getting%20Started/Wiki%20Configuration).
9. Record design decisions in Slack by tagging messages with the Slackbot ([Using the Slackbot](https://gitlab.com/mde4asd/protoslackbot/-/wikis/Getting%20Started/Bot%20Use)).

** These are unnecessary with a static VM address, the address will be copied once only.  


## Deploying locally with docker image:  

***

1. Start ngrok `ngrok http 3000`
2. Run `make docker-prod`
3. Run `make run`
4. Copy https:// address from ngrok to [Slack Event Subscriptions](https://api.slack.com/apps/A026WE440RH/event-subscriptions?) `https://....ngrok.io/slack/events` and save
5. Due to checkbox interactivity there is a requirement to need to add it to [Slack Interactivity & Shortcuts](https://api.slack.com/apps/A026WE440RH/interactive-messages?) for checkboxes.
6. (optional) set up the Slackbot to output recorded design decisions to a [Gitlab wiki](https://gitlab.com/mde4asd/protoslackbot/-/wikis/Getting%20Started/Wiki%20Configuration).
7. Record design decisions in Slack by tagging messages with the Slackbot ([Using the Slackbot](https://gitlab.com/mde4asd/protoslackbot/-/wikis/Getting%20Started/Bot%20Use)).

[Manage Slack Apps](https://api.slack.com/apps) -
[New Slack Management Panel](https://app.slack.com/app-settings/T01QWLE5S87/A026WE440RH/general) -
[Searching For Design Decisions](https://gitlab.com/mde4asd/protoslackbot/-/wikis/Getting%20Started/Searching) -
[Amending Design Decisions](https://gitlab.com/mde4asd/protoslackbot/-/wikis/Getting%20Started/Amending%20Design%20Decisions)


## Troubleshooting  

***

Error with   
- pytorch-lightning may be due to old version of [https://github.com/YosefLab/scvi-tools/issues/1029](https://github.com/YosefLab/scvi-tools/issues/1029)  
- torch-audio try `pip3 freeze --user | xargs pip3 uninstall -y` to remove unnecessary junk` and reinstall dependencies  

## Unit tests

***

To run the unit tests the command `python -m unittest` can be used. For the tests to be picked up, the name of a 
test file must be of the form "test_*.py" (for example, "test_App.py"). Sub-directories inside the test directory must 
also contain an "`__init__.py`" file.
