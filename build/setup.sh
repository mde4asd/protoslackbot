#!/usr/bin/zsh
#
python3 -m pip install --upgrade pip
#
pip3 install -r build/requirements.txt
#
python3 -m spacy download en_core_web_sm